﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACCServerGUI
{
    public class Aids
    {
        //Variables
        private int _stabilityControlLevelMax;
        private int _disableAutosteer;
        private int _disableIdealLine;
        private int _disableAutoPitLimiter;
        private int _disableAutoGear;
        private int _disableAutoClutch;
        private int _disableAutoEngineStart;
        private int _disableAutoWiper;
        private int _disableAutoLights;

        //Constructors
        public Aids() { }
        public Aids(
             int stabilityControlLevelMax,
             int disableAutosteer,
             int disableIdealLine,
             int disableAutoPitLimiter,
             int disableAutoGear,
             int disableAutoClutch,
             int disableAutoEngineStart,
             int disableAutoWiper,
             int disableAutoLights
            )
        {
            this.stabilityControlLevelMax = stabilityControlLevelMax;
            this.disableAutosteer = disableAutosteer;
            this.disableIdealLine = disableIdealLine;
            this.disableAutoPitLimiter = disableAutoPitLimiter;
            this.disableAutoGear = disableAutoGear;
            this.disableAutoClutch = disableAutoClutch;
            this.disableAutoEngineStart = disableAutoEngineStart;
            this.disableAutoWiper = disableAutoWiper;
            this.disableAutoLights = disableAutoLights;
        }
        //Getters and Setters
        public int stabilityControlLevelMax { get => _stabilityControlLevelMax; set => _stabilityControlLevelMax = value; }
        public int disableAutosteer { get => _disableAutosteer; set => _disableAutosteer = value; }
        public int disableIdealLine { get => _disableIdealLine; set => _disableIdealLine = value; }
        public int disableAutoPitLimiter { get => _disableAutoPitLimiter; set => _disableAutoPitLimiter = value; }
        public int disableAutoGear { get => _disableAutoGear; set => _disableAutoGear = value; }
        public int disableAutoClutch { get => _disableAutoClutch; set => _disableAutoClutch = value; }
        public int disableAutoEngineStart { get => _disableAutoEngineStart; set => _disableAutoEngineStart = value; }
        public int disableAutoWiper { get => _disableAutoWiper; set => _disableAutoWiper = value; }
        public int disableAutoLights { get => _disableAutoLights; set => _disableAutoLights = value; }
    }
}
