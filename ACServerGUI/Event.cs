﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACCServerGUI
{
    class Event
    {
        private String _track;
        private String _eventType;
        private int _preRaceWaitingTimeSeconds;
        private int _sessionOverTimeSeconds;
        private int _ambientTemp;
        private double _cloudLevel;
        private double _rain;
        private int _weatherRandomness;
        private int _postQualySeconds;
        private int _postRaceSeconds;
        private BindingList<Session> _sessions;
        private int _configVersion;
        public Event() { }
        public Event(String track,
                    String eventType,
                    int preRaceWaitingTimeSeconds,
                    int sessionOverTimeSeconds,
                    int ambientTemp,
                    double cloudLevel,
                    double rain,
                    int weatherRandomness,
                    int _postQualySeconds,
                    int _postRaceSeconds,
                    int configVersion,
                    BindingList<Session> sessions)
        {
            this.track = track;
            this.eventType = eventType;
            this.preRaceWaitingTimeSeconds = preRaceWaitingTimeSeconds;
            this.sessionOverTimeSeconds = sessionOverTimeSeconds;
            this.ambientTemp = ambientTemp;
            this.cloudLevel = cloudLevel;
            this.rain = rain;
            this.weatherRandomness = weatherRandomness;
            this.sessions = sessions;
            this.configVersion = configVersion;
        }
        public String track { get => _track; set => _track = value; }
        public String eventType { get => _eventType; set => _eventType = value; }
        public int preRaceWaitingTimeSeconds { get => _preRaceWaitingTimeSeconds; set => _preRaceWaitingTimeSeconds = value; }
        public int sessionOverTimeSeconds { get => _sessionOverTimeSeconds; set => _sessionOverTimeSeconds = value; }
        public int ambientTemp { get => _ambientTemp; set => _ambientTemp = value; }
        public double cloudLevel { get => _cloudLevel; set => _cloudLevel = value; }
        public double rain { get => _rain; set => _rain = value; }
        public int weatherRandomness { get => _weatherRandomness; set => _weatherRandomness = value; }
        public int postQualySeconds { get => _postQualySeconds; set => _postQualySeconds = value; }
        public int postRaceSeconds { get => _postRaceSeconds; set => _postRaceSeconds = value; }
        public BindingList<Session> sessions { get => _sessions; set => _sessions = value; }
        public int configVersion { get => _configVersion; set => _configVersion = value; }
    }
}
