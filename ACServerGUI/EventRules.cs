﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACCServerGUI
{
    public class EventRules
    {
        //Variables in inglish
        private int _qualifyStandingType;
        private int _superpoleMaxCar;
        private int _pitWindowLengthSec;
        private int _driverStintTimeSec;
        private bool _isRefuellingAllowedInRace;
        private bool _isRefuellingTimeFixed;
        private int _maxDriversCount;
        private int _mandatoryPitstopCount;
        private int _maxTotalDrivingTime;
        private bool _isMandatoryPitstopRefuellingRequired;
        private bool _isMandatoryPitstopTyreChangeRequired;
        private bool _isMandatoryPitstopSwapDriverRequired;
        private int _tyreSetCount;

        //Constructors
        public EventRules()
        {

        }
        public EventRules(
            int qualifyStandingType,
            int superpoleMaxCar,
            int pitWindowLengthSec,
            int driverStintTimeSec,
            bool isRefuellingAllowedInRace,
            bool isRefuellingTimeFixed,
            int maxDriversCount,
            int mandatoryPitstopCount,
            int maxTotalDrivingTime,
            bool isMandatoryPitstopRefuellingRequired,
            bool isMandatoryPitstopTyreChangeRequired,
            bool isMandatoryPitstopSwapDriverRequired,
            int tyreSetCount
            )
        {
            this.qualifyStandingType = qualifyStandingType;
            this.superpoleMaxCar = superpoleMaxCar;
            this.pitWindowLengthSec = pitWindowLengthSec;
            this.driverStintTimeSec = driverStintTimeSec;
            this.isRefuellingAllowedInRace = isRefuellingAllowedInRace;
            this.isRefuellingTimeFixed = isRefuellingTimeFixed;
            this.maxDriversCount = maxDriversCount;
            this.mandatoryPitstopCount = mandatoryPitstopCount;
            this.maxTotalDrivingTime = maxTotalDrivingTime;
            this.isMandatoryPitstopRefuellingRequired = isMandatoryPitstopRefuellingRequired;
            this.isMandatoryPitstopTyreChangeRequired = isMandatoryPitstopTyreChangeRequired;
            this.isMandatoryPitstopSwapDriverRequired = isMandatoryPitstopSwapDriverRequired;
            this.tyreSetCount = tyreSetCount;
        }



        //Getters and Setters
        public int qualifyStandingType { get => _qualifyStandingType; set => _qualifyStandingType = value; }
        public int superpoleMaxCar { get => _superpoleMaxCar; set => _superpoleMaxCar = value; }
        public int pitWindowLengthSec { get => _pitWindowLengthSec; set => _pitWindowLengthSec = value; }
        public int driverStintTimeSec { get => _driverStintTimeSec; set => _driverStintTimeSec = value; }
        public bool isRefuellingAllowedInRace { get => _isRefuellingAllowedInRace; set => _isRefuellingAllowedInRace = value; }
        public bool isRefuellingTimeFixed { get => _isRefuellingTimeFixed; set => _isRefuellingTimeFixed = value; }
        public int maxDriversCount { get => _maxDriversCount; set => _maxDriversCount = value; }
        public int mandatoryPitstopCount { get => _mandatoryPitstopCount; set => _mandatoryPitstopCount = value; }
        public int maxTotalDrivingTime { get => _maxTotalDrivingTime; set => _maxTotalDrivingTime = value; }
        public bool isMandatoryPitstopRefuellingRequired { get => _isMandatoryPitstopRefuellingRequired; set => _isMandatoryPitstopRefuellingRequired = value; }
        public bool isMandatoryPitstopTyreChangeRequired { get => _isMandatoryPitstopTyreChangeRequired; set => _isMandatoryPitstopTyreChangeRequired = value; }
        public bool isMandatoryPitstopSwapDriverRequired { get => _isMandatoryPitstopSwapDriverRequired; set => _isMandatoryPitstopSwapDriverRequired = value; }
        public int tyreSetCount { get => _tyreSetCount; set => _tyreSetCount = value; }
    }
}
