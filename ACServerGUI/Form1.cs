﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;

namespace ACCServerGUI
{
    public partial class Form1 : Form
    {
        Conexiones conexiones = new Conexiones();
        Configuracion configuracion = new Configuracion();
        Event evento = new Event();
        //Aids aids = new Aids();
        /// <summary>
        /// Lista con las pistas disponibles para el dropdown de dropPistas
        /// </summary>
        BindingList<Pista> pistasDisponibles;
        BindingList<String> tipoDeSesion;
        BindingList<Dia> dias;
        BindingList<Session> programmedSessions;
        MyTooltips tooltips = new MyTooltips();
        public Form1()
        {
            InitializeComponent();
            //Arreglo de pistas para el combobox controlPistas
            pistasDisponibles = new BindingList<Pista>()
            {
                new Pista() { Nombre="Monza", Value="monza" },
                new Pista() { Nombre="Zolder", Value="zolder" },
                new Pista() { Nombre="Brands Hatch", Value="brands_hatch" },
                new Pista() { Nombre="Silverstone", Value="silverstone" },
                new Pista() { Nombre="Paul Ricard", Value="paul_ricard" },
                new Pista() { Nombre="Misano", Value="misano" },
                new Pista() { Nombre="Spa-Francorchamps", Value="spa" },
                new Pista() { Nombre="Nurbrugring", Value="nurburgring" },
                new Pista() { Nombre="Barcelona", Value="barcelona" },
                new Pista() { Nombre="Hungaroring", Value="hungaroring" },
                new Pista() { Nombre="Zandvoort", Value="zandvoort" },
                new Pista() { Nombre="Monza 2019", Value="monza_2019" },
                new Pista() { Nombre="Zolder 2019", Value="zolder_2019" },
                new Pista() { Nombre="Brands Hatch 2019", Value="brands_hatch_2019" },
                new Pista() { Nombre="Silverstone 2019", Value="silverstone_2019" },
                new Pista() { Nombre="Paul Ricard 2019", Value="paul_ricard_2019" },
                new Pista() { Nombre="Misano 2019", Value="misano_2019" },
                new Pista() { Nombre="Spa-Francorchamps 2019", Value="spa_2019" },
                new Pista() { Nombre="Nurburgring 2019", Value="nurburgring_2019" },
                new Pista() { Nombre="Barcelona 2019", Value="barcelona_2019" },
                new Pista() { Nombre="Hungaroring 2019", Value="hungaroring_2019" },
                new Pista() { Nombre="Zandvoort 2019", Value="zandvoort_2019" }
            };
            /** Ya que ListControl del combobox controlPistas contiene objetos Pista,
             * establece que mostrara la propiedad nombre de el objeto Pista al desplegarse
             * la lista. **/
            controlTrack.ValueMember = null;
            controlTrack.DisplayMember = "Nombre";
            //Establece la fuente de datos para el combobox controlPistas
            controlTrack.DataSource = pistasDisponibles;
            //controlPistas AUTOCOMPLETAR
            controlTrack.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            controlTrack.AutoCompleteSource = AutoCompleteSource.ListItems;
            controlTrack.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //Arreglo de tipos de sesión para controlTipoDeSesion
            tipoDeSesion = new BindingList<string>()
            {
                { "P" },
                { "Q" },
                { "R" }
            };
            controlSessionType.DataSource = tipoDeSesion;
            //Arreglo de dias de fin de semana para controlDiaDeSesion
            dias = new BindingList<Dia>()
            {
                new Dia() { NombreDia="Viernes", Valor=1 },
                new Dia() { NombreDia="Sábado", Valor=2 },
                new Dia() { NombreDia="Domingo", Valor=3 }
            };
            controlDayOfWeekend.ValueMember = null;
            controlDayOfWeekend.DisplayMember = "NombreDia";
            controlDayOfWeekend.DataSource = dias;
            ////////////////////////////////////TABLA DE SESIONES 1
            //Arreglo de sesiones para control controlSessionList
            programmedSessions = new BindingList<Session>()
            {
                /*
                new Session() { HourOfDay=5, DayOfWeekend=1, TimeMultiplier=1, SessionType="P", SessionDurationMinutes=30},
                new Session() { HourOfDay=5, DayOfWeekend=2, TimeMultiplier=1, SessionType="Q", SessionDurationMinutes=15},
                new Session() { HourOfDay=5, DayOfWeekend=3, TimeMultiplier=1, SessionType="R", SessionDurationMinutes=60}
                */
            };
            ////////////////////////////////////////////////////////
            /////////////////////////////////////TABLA DE SESIONES 2
            controlSessionsTable.DataSource = programmedSessions;
            ////////////////////////////////////////////////////////
            /* Eta porcion de codigo hace lo mismo que AUTOCOMPLETAR, se dejo de usar
             * ya que la anterior es mas corta.
            AutoCompleteStringCollection collection = new AutoCompleteStringCollection();
            foreach (var item in pistasDisponibles)
            {
                collection.Add(item.nombre);
            }
            controlPistas.AutoCompleteCustomSource = collection;
            controlPistas.AutoCompleteSource = AutoCompleteSource.CustomSource;
            controlPistas.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            */
            //Ayudas visuales para los controles de la interfaz
            this.tabs.ShowToolTips = true;
            this.Conexiones.ToolTipText = "Conexiones al servidor: tcp udp etc.";
            this.Configuracion.ToolTipText = "Configuración del servidor: Nombre, pass, rating, medallas, etc.";
            this.Evento.ToolTipText = "Configuración fin carrera: Pista, hora inicio, tiempos de sesiones, etc.";
            this.Reglas.ToolTipText = "Reglas de pitstops: Mínimo de paradas, repostaje/sin repostaje, etc.";
            this.Text = AppDomain.CurrentDomain.BaseDirectory;
            this.Size = new Size(500, 420);
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            precarga();
            loadTooltips();
        }
        private class Dia
        {
            private String _nombreDia;
            private int _valor;
            public Dia() { }
            public Dia(String nombreDia,
                        int valor)
            {
                NombreDia = nombreDia;
                Valor = valor;
            }
            public String NombreDia { get => _nombreDia; set => _nombreDia = value; }
            public int Valor { get => _valor; set => _valor = value; }
        }
        public void actualizarConexiones()
        {
            conexiones.udpPort = (int)controlUDP.Value;
            conexiones.tcpPort = (int)controlTCP.Value;
            conexiones.maxConnections = (int)controlMaxConn.Value;
            conexiones.lanDiscovery = (controlLANDiscovery.Checked) ? 1 : 0;
            conexiones.registerToLobby = (controlRegisterToLobby1.Checked) ? 1 : 0;
            conexiones.configVersion = 1;
        }
        public void actualizarConfiguracion()
        {
            configuracion.serverName = controlServerName.Text;
            configuracion.adminPassword = controlAdminPassword.Text;
            configuracion.trackMedalsRequirement = controlMedals.Value;
            configuracion.racecraftRatingRequirement = controlRacecraft.Value;
            configuracion.password = controlPassword.Text;
            configuracion.spectatorPassword = controlSpectatorPassword.Text;
            configuracion.maxCarSlots = (int)controlMaxCarSlots.Value;
            configuracion.dumpLeaderboards = (controlDumpLeaderboard.Checked) ? 1 : 0;
            configuracion.isRaceLocked = (controlIsRaceLocked.Checked) ? 1 : 0;
            configuracion.randomizeTrackWhenEmpty = (controlRadomizeTrackWhenEmpty.Checked) ? 1 : 0;
            configuracion.centralEntryListPath = "";
            configuracion.allowAutoDQ = (controlAllowAutoDQ.Checked) ? 1 : 0;
            configuracion.shortFormationLap = (controlShortFormationLap.Checked) ? 1 : 0;
            configuracion.dumpEntryList = 0;
        }
        public void actualizarEvento()
        {
            evento.track = ((Pista)controlTrack.SelectedItem).Value;
            evento.eventType = "";
            evento.preRaceWaitingTimeSeconds = (int)controlPreRaceWaitingSeconds.Value;
            evento.sessionOverTimeSeconds = (int)controlSessionOverTimeSeconds.Value;
            evento.ambientTemp = (int)controlAmbientTemp.Value;
            evento.cloudLevel = (double)controlCloudLevel.Value / 10;
            evento.rain = (double)controlRain.Value / 10;
            evento.weatherRandomness = (int)controlWeatherRandomness.Value;
            evento.postQualySeconds = (int)controlPostQualySeconds.Value;
            evento.postRaceSeconds = (int)controlPostRaceSeconds.Value;
            evento.sessions = programmedSessions;
            evento.configVersion = 1;
        }
        private void controlMedals_ValueChanged(object sender, EventArgs e)
        {
            valueMedals.Text = controlMedals.Value.ToString();
        }
        private void controlSafety_ValueChanged(object sender, EventArgs e)
        {
            valueSafety.Text = controlSafety.Value.ToString();
        }
        private void controlRacecraft_ValueChanged(object sender, EventArgs e)
        {
            valueRacecraft.Text = controlRacecraft.Value.ToString();
        }
        private void controlIniciar_Click(object sender, EventArgs e)
        {
            accServer = Process.Start(AppDomain.CurrentDomain.BaseDirectory + "/accServer.exe");
            controlIniciar.Enabled = false;
        }
        private void controlDetener_Click(object sender, EventArgs e)
        {
            if (!accServer.HasExited)
                accServer.Kill();
            controlIniciar.Enabled = true;
        }
        private void controlActualizar_Click(object sender, EventArgs e)
        {
            if (controlIniciar.Enabled) {
                actualizarConexiones();
                actualizarConfiguracion();
                actualizarEvento();

                Console.WriteLine(System.IO.Path.GetDirectoryName(Application.ExecutablePath));

                // serialize JSON to a string and then write string to a file
                File.WriteAllText(@"" + System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "/cfg/configuration.json", JsonConvert.SerializeObject(conexiones));
                using (StreamWriter file = File.CreateText(@"" + System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "/cfg/configuration.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, conexiones);
                }
                File.WriteAllText(@"" + System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "/cfg/settings.json", JsonConvert.SerializeObject(configuracion));
                using (StreamWriter file = File.CreateText(@"" + System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "/cfg/settings.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, configuracion);
                }
                File.WriteAllText(@"" + System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "/cfg/event.json", JsonConvert.SerializeObject(configuracion));
                using (StreamWriter file = File.CreateText(@"" + System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "/cfg/event.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, evento);
                }
            } else
            {
                MessageBox.Show("Detener primero el servidor!", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void controlAgregarSesion_Click(object sender, EventArgs e)
        {
            programmedSessions.Add(new Session()
            {
                HourOfDay = (int)controlHourOfDay.Value,
                DayOfWeekend = ((Dia)controlDayOfWeekend.SelectedItem).Valor,
                TimeMultiplier = (int)controlTimeMultiplier.Value,
                SessionType = controlSessionType.SelectedItem.ToString(),
                SessionDurationMinutes = (int)controlSessionDuration.Value
            });
            controlSessionsTable.Refresh();
            controlSessionsTable.DataSource = programmedSessions;
            controlSessionsTable.Refresh();
            controlSessionsTable.Update();
        }
        private void precarga()
        {
            //Precarga de configuration.json
            using (StreamReader readerConfiguration = new StreamReader(
                AppDomain.CurrentDomain.BaseDirectory + "/cfg/configuration.json")
                )
            {
                String configurationJSON = readerConfiguration.ReadToEnd();
                conexiones = JsonConvert.DeserializeObject<Conexiones>(configurationJSON);
                this.controlUDP.Value = conexiones.udpPort;
                this.controlTCP.Value = conexiones.tcpPort;
                this.controlMaxConn.Value = conexiones.maxConnections;
                this.controlLANDiscovery.Checked = (conexiones.lanDiscovery == 1) ? true : false;
                this.controlRegisterToLobby0.Checked = (conexiones.registerToLobby == 1) ? true : false;
                //this.controlServerName.Text=(conexiones.)
            }
            //Precarga de settings.json
            using (StreamReader readerSettings = new StreamReader(
                AppDomain.CurrentDomain.BaseDirectory + "/cfg/settings.json")
                )
            {

            }
            //Precarga de event.json
            using (StreamReader readerEvent = new StreamReader(
                AppDomain.CurrentDomain.BaseDirectory + "/cfg/event.json")
                )
            {
                String eventJSON = readerEvent.ReadToEnd();
                evento = JsonConvert.DeserializeObject<Event>(eventJSON);
                this.controlTrack.SelectedIndex =
                    (evento.track.Equals("monza")) ? 0 :
                    (evento.track.Equals("zolder")) ? 1 :
                    (evento.track.Equals("brands_hatch")) ? 2 :
                    (evento.track.Equals("silverstone")) ? 3 :
                    (evento.track.Equals("paul_ricard")) ? 4 :
                    (evento.track.Equals("misano")) ? 5 :
                    (evento.track.Equals("spa")) ? 6 :
                    (evento.track.Equals("nurburgring")) ? 7 :
                    (evento.track.Equals("barcelona")) ? 8 :
                    (evento.track.Equals("hungaroring")) ? 9 :
                    (evento.track.Equals("zandvoort")) ? 10 :
                    (evento.track.Equals("monza_2019")) ? 11 :
                    (evento.track.Equals("zolder_2019")) ? 12 :
                    (evento.track.Equals("brands_hatch_2019")) ? 13 :
                    (evento.track.Equals("silverstone_2019")) ? 14 :
                    (evento.track.Equals("paul_ricard_2019")) ? 15 :
                    (evento.track.Equals("misano_2019")) ? 16 :
                    (evento.track.Equals("spa_2019")) ? 17 :
                    (evento.track.Equals("nurburgring_2019")) ? 18 :
                    (evento.track.Equals("barcelona_2019")) ? 19 :
                    (evento.track.Equals("hungaroring_2019")) ? 20 : 21;
                this.controlPreRaceWaitingSeconds.Value = evento.preRaceWaitingTimeSeconds;
                this.controlSessionOverTimeSeconds.Value = evento.sessionOverTimeSeconds;
                this.controlPostQualySeconds.Value = evento.postQualySeconds;
                this.controlPostRaceSeconds.Value = evento.postRaceSeconds;
                //Actualizar label manualmente
                this.controlAmbientTemp.Value = evento.ambientTemp;
                this.valueAmbientTemp.Text = this.controlAmbientTemp.Value.ToString();
                //Actualizar label manualmente
                this.controlCloudLevel.Value = (int)(evento.cloudLevel * 10);
                this.valueCloudLevel.Text = "" + (double)controlCloudLevel.Value / 10;
                //Actualizar label manualmente
                this.controlRain.Value = (int)(evento.rain * 10);
                this.valueRain.Text = "" + (double)controlRain.Value / 10;
                //Actualizar label manualmente
                this.controlWeatherRandomness.Value = evento.weatherRandomness;
                this.valueWeatherRandomness.Text = controlWeatherRandomness.Value.ToString();
            }
        }
        public void loadTooltips()
        {
            using (StreamReader readerTooltips = new StreamReader(
                AppDomain.CurrentDomain.BaseDirectory + "/Tooltips.json")
                )
            {
                String tooltipsJSON = readerTooltips.ReadToEnd();
                tooltips = JsonConvert.DeserializeObject<MyTooltips>(tooltipsJSON);
                
                ToolTip tooltip = new ToolTip();
                tooltip.ToolTipIcon = ToolTipIcon.None;
                tooltip.IsBalloon = true;
                tooltip.ShowAlways = true;
                tooltip.SetToolTip(labelUDP, tooltips.udpPort);
                tooltip.SetToolTip(labelTCP, tooltips.tcpPort);
                tooltip.SetToolTip(labelMaxConn, tooltips.maxConnections);
                tooltip.SetToolTip(controlLANDiscovery, tooltips.lanDiscovery);
                tooltip.SetToolTip(controlRegisterToLobby0, tooltips.registerToLobby);
                tooltip.SetToolTip(controlRegisterToLobby1, tooltips.registerToLobby);
                tooltip.SetToolTip(labelServerName, tooltips.serverName);
                tooltip.SetToolTip(labelAdminPassword, tooltips.adminPassword);
                tooltip.SetToolTip(labelPassword, tooltips.password);
                tooltip.SetToolTip(labelSpectatorPassword, tooltips.spectatorPassword);
                tooltip.SetToolTip(labelMaxCarSlots, tooltips.maxCarSlots);
                tooltip.SetToolTip(labelPassword, tooltips.password);
                tooltip.SetToolTip(labelPassword, tooltips.password);
            }
        }
        private void controlAmbientTemp_ValueChanged(object sender, EventArgs e)
        {
            valueAmbientTemp.Text = controlAmbientTemp.Value.ToString();
        }
        private void controlCloudLevel_ValueChanged(object sender, EventArgs e)
        {
            valueCloudLevel.Text = "" + (double) controlCloudLevel.Value / 10;
        }
        private void controlRain_ValueChanged(object sender, EventArgs e)
        {
            valueRain.Text = "" + (double) controlRain.Value / 10;
        }
        private void controlWeatherRandomness_ValueChanged(object sender, EventArgs e)
        {
            valueWeatherRandomness.Text = controlWeatherRandomness.Value.ToString();
        }
    }
}
