﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACCServerGUI
{
    public class MyTooltips
    {
        public MyTooltips() { }
        public string tcpPort { get; set; }
        public string udpPort { get; set; }
        public string registerToLobby { get; set; }
        public string maxConnections { get; set; }
        public string lanDiscovery { get; set; }
        public string configVersion { get; set; }
        public string serverName { get; set; }
        public string adminPassword { get; set; }
        public string trackMedalsRequirement { get; set; }
        public string safetyRatingRequirement { get; set; }
        public string racecraftRatingRequirement { get; set; }
        public string password { get; set; }
        public string spectatorPassword { get; set; }
        public string maxCarSlots { get; set; }
        public string dumpLeaderboards { get; set; }
        public string isRaceLocked { get; set; }
        public string randomizeTrackWhenEmpty { get; set; }
        public string centralEntryListPath { get; set; }
        public string allowAutoDQ { get; set; }
        public string shortFormationLap { get; set; }
        public string dumpEntryList { get; set; }
        public string track { get; set; }
        public string preRaceWaitingTimeSeconds { get; set; }
        public string sessionOverTimeSeconds { get; set; }
        public string ambientTemp { get; set; }
        public string cloudLevel { get; set; }
        public string rain { get; set; }
        public string weatherRandomness { get; set; }
        public string postQualySeconds { get; set; }
        public string postRaceSeconds { get; set; }
        public string sessions { get; set; }
        public string hourOfDay { get; set; }
        public string dayOfWeekend { get; set; }
        public string timeMultiplier { get; set; }
        public string sessionType { get; set; }
        public string sessionDurationMinutes { get; set; }
        public string qualifyStandingType { get; set; }
        public string superpoleMaxCar { get; set; }
        public string pitWindowLengthSec { get; set; }
        public string driverStintTimeSec { get; set; }
        public string isRefuellingAllowedInRace { get; set; }
        public string isRefuellingTimeFixed { get; set; }
        public string mandatoryPitstopCount { get; set; }
        public string maxTotalDrivingTime { get; set; }
        public string maxDriversCount { get; set; }
        public string isMandatoryPitstopRefuellingRequired { get; set; }
        public string isMandatoryPitstopTyreChangeRequired { get; set; }
        public string isMandatoryPitstopSwapDriverRequired { get; set; }
        public string tyreSetCount { get; set; }
        public string entries { get; set; }
        public string drivers { get; set; }
        public string raceNumber { get; set; }
        public string forcedCarModel { get; set; }
        public string overrideDriverInfo { get; set; }
        public string customCar { get; set; }
        public string overrideCarModelForCustomCar { get; set; }
        public string isServerAdmin { get; set; }
        public string defaultGridPosition { get; set; }
        public string ballastkg { get; set; }
        public string restrictor { get; set; }
        public string forceEntryList { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string shortName { get; set; }
        public string driverCategory { get; set; }
        public string playerID { get; set; }
        public string carModel { get; set; }
        public string ballast { get; set; }
    }
}

