﻿using System.Diagnostics;

namespace ACCServerGUI
{
    partial class Form1
    {
        Process accServer;
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabs = new System.Windows.Forms.TabControl();
            this.Conexiones = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.controlMaxCarSlots = new System.Windows.Forms.NumericUpDown();
            this.labelMaxCarSlots = new System.Windows.Forms.Label();
            this.controlServerName = new System.Windows.Forms.TextBox();
            this.controlSpectatorPassword = new System.Windows.Forms.TextBox();
            this.labelServerName = new System.Windows.Forms.Label();
            this.controlPassword = new System.Windows.Forms.TextBox();
            this.labelAdminPassword = new System.Windows.Forms.Label();
            this.controlAdminPassword = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.labelSpectatorPassword = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.controlMaxConn = new System.Windows.Forms.NumericUpDown();
            this.controlLANDiscovery = new System.Windows.Forms.CheckBox();
            this.controlRegisterToLobby1 = new System.Windows.Forms.RadioButton();
            this.controlRegisterToLobby0 = new System.Windows.Forms.RadioButton();
            this.labelMaxConn = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.controlTCP = new System.Windows.Forms.NumericUpDown();
            this.controlUDP = new System.Windows.Forms.NumericUpDown();
            this.labelUDP = new System.Windows.Forms.Label();
            this.labelTCP = new System.Windows.Forms.Label();
            this.Configuracion = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.controlShortFormationLap = new System.Windows.Forms.CheckBox();
            this.controlAllowAutoDQ = new System.Windows.Forms.CheckBox();
            this.controlRadomizeTrackWhenEmpty = new System.Windows.Forms.CheckBox();
            this.controlIsRaceLocked = new System.Windows.Forms.CheckBox();
            this.controlDumpLeaderboard = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.valueMedals = new System.Windows.Forms.Label();
            this.valueRacecraft = new System.Windows.Forms.Label();
            this.valueSafety = new System.Windows.Forms.Label();
            this.controlRacecraft = new System.Windows.Forms.TrackBar();
            this.controlSafety = new System.Windows.Forms.TrackBar();
            this.controlMedals = new System.Windows.Forms.TrackBar();
            this.labelRacecraft = new System.Windows.Forms.Label();
            this.labelSafety = new System.Windows.Forms.Label();
            this.labelMedals = new System.Windows.Forms.Label();
            this.Evento = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.controlTrack = new System.Windows.Forms.ComboBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.controlPostRaceSeconds = new System.Windows.Forms.NumericUpDown();
            this.controlPostQualySeconds = new System.Windows.Forms.NumericUpDown();
            this.controlSessionOverTimeSeconds = new System.Windows.Forms.NumericUpDown();
            this.controlPreRaceWaitingSeconds = new System.Windows.Forms.NumericUpDown();
            this.labelPostRaceSeconds = new System.Windows.Forms.Label();
            this.labelPostQualySeconds = new System.Windows.Forms.Label();
            this.labelSessionOverTimeSeconds = new System.Windows.Forms.Label();
            this.labelPreRaceWaitingSeconds = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.valueWeatherRandomness = new System.Windows.Forms.Label();
            this.valueRain = new System.Windows.Forms.Label();
            this.valueCloudLevel = new System.Windows.Forms.Label();
            this.valueAmbientTemp = new System.Windows.Forms.Label();
            this.controlWeatherRandomness = new System.Windows.Forms.TrackBar();
            this.controlRain = new System.Windows.Forms.TrackBar();
            this.controlCloudLevel = new System.Windows.Forms.TrackBar();
            this.controlAmbientTemp = new System.Windows.Forms.TrackBar();
            this.labelAmbientTemp = new System.Windows.Forms.Label();
            this.labelRain = new System.Windows.Forms.Label();
            this.labelWeatherRandomness = new System.Windows.Forms.Label();
            this.labelCloudLevel = new System.Windows.Forms.Label();
            this.FinDeSemana = new System.Windows.Forms.TabPage();
            this.controlSessionsTable = new System.Windows.Forms.DataGridView();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.controlDayOfWeekend = new System.Windows.Forms.ComboBox();
            this.controlEliminarSesion = new System.Windows.Forms.Button();
            this.controlActualizarSesion = new System.Windows.Forms.Button();
            this.controlAgregarSesion = new System.Windows.Forms.Button();
            this.controlSessionDuration = new System.Windows.Forms.NumericUpDown();
            this.labelSessionDuration = new System.Windows.Forms.Label();
            this.controlSessionType = new System.Windows.Forms.ComboBox();
            this.controlTimeMultiplier = new System.Windows.Forms.NumericUpDown();
            this.controlHourOfDay = new System.Windows.Forms.NumericUpDown();
            this.labelSessionType = new System.Windows.Forms.Label();
            this.labelTimeMultiplier = new System.Windows.Forms.Label();
            this.labelDayOfWeekend = new System.Windows.Forms.Label();
            this.labelHourOfDay = new System.Windows.Forms.Label();
            this.Reglas = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.tyreSetCount = new System.Windows.Forms.TrackBar();
            this.label13 = new System.Windows.Forms.Label();
            this.isMandatoryPitstopSwapDriverRequired = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.isMandatoryPitstopTyreChangeRequired = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.isMandatoryPitstopRefuellingRequired = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.maxDriversCount = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.maxTotalDrivingTime = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.mandatoryPitstopCount = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.driverStintTimeSec = new System.Windows.Forms.NumericUpDown();
            this.pitWindowLengthSec = new System.Windows.Forms.NumericUpDown();
            this.isRefuellingTimeFixed = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.isRefuellingAllowedInRace = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.qualifyStandingType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Ayudas = new System.Windows.Forms.TabPage();
            this.disableAutoLights = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.disableAutoWiper = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.disableAutoEngineStart = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.disableAutoClutch = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.disableAutoGear = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.disableAutoPitLimiter = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.disableIdealLine = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.disableAutosteer = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.stabilityControlLevelMax = new System.Windows.Forms.TrackBar();
            this.label15 = new System.Windows.Forms.Label();
            this.process1 = new System.Diagnostics.Process();
            this.controlIniciar = new System.Windows.Forms.Button();
            this.controlDetener = new System.Windows.Forms.Button();
            this.controlActualizar = new System.Windows.Forms.Button();
            this.tabs.SuspendLayout();
            this.Conexiones.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlMaxCarSlots)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlMaxConn)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlTCP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlUDP)).BeginInit();
            this.Configuracion.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlRacecraft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlSafety)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlMedals)).BeginInit();
            this.Evento.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlPostRaceSeconds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlPostQualySeconds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlSessionOverTimeSeconds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlPreRaceWaitingSeconds)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlWeatherRandomness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlRain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlCloudLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlAmbientTemp)).BeginInit();
            this.FinDeSemana.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlSessionsTable)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlSessionDuration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlTimeMultiplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlHourOfDay)).BeginInit();
            this.Reglas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tyreSetCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxDriversCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxTotalDrivingTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mandatoryPitstopCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.driverStintTimeSec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pitWindowLengthSec)).BeginInit();
            this.Ayudas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stabilityControlLevelMax)).BeginInit();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabs.Controls.Add(this.Conexiones);
            this.tabs.Controls.Add(this.Configuracion);
            this.tabs.Controls.Add(this.Evento);
            this.tabs.Controls.Add(this.FinDeSemana);
            this.tabs.Controls.Add(this.Reglas);
            this.tabs.Controls.Add(this.Ayudas);
            this.tabs.Location = new System.Drawing.Point(10, 10);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(458, 339);
            this.tabs.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabs.TabIndex = 0;
            // 
            // Conexiones
            // 
            this.Conexiones.Controls.Add(this.groupBox3);
            this.Conexiones.Controls.Add(this.groupBox2);
            this.Conexiones.Controls.Add(this.groupBox1);
            this.Conexiones.Location = new System.Drawing.Point(4, 25);
            this.Conexiones.Name = "Conexiones";
            this.Conexiones.Padding = new System.Windows.Forms.Padding(3);
            this.Conexiones.Size = new System.Drawing.Size(450, 310);
            this.Conexiones.TabIndex = 0;
            this.Conexiones.Text = "Conexiones";
            this.Conexiones.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.controlMaxCarSlots);
            this.groupBox3.Controls.Add(this.labelMaxCarSlots);
            this.groupBox3.Controls.Add(this.controlServerName);
            this.groupBox3.Controls.Add(this.controlSpectatorPassword);
            this.groupBox3.Controls.Add(this.labelServerName);
            this.groupBox3.Controls.Add(this.controlPassword);
            this.groupBox3.Controls.Add(this.labelAdminPassword);
            this.groupBox3.Controls.Add(this.controlAdminPassword);
            this.groupBox3.Controls.Add(this.labelPassword);
            this.groupBox3.Controls.Add(this.labelSpectatorPassword);
            this.groupBox3.Location = new System.Drawing.Point(10, 120);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(440, 190);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Lobby";
            // 
            // controlMaxCarSlots
            // 
            this.controlMaxCarSlots.Location = new System.Drawing.Point(195, 130);
            this.controlMaxCarSlots.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.controlMaxCarSlots.Name = "controlMaxCarSlots";
            this.controlMaxCarSlots.Size = new System.Drawing.Size(202, 20);
            this.controlMaxCarSlots.TabIndex = 22;
            // 
            // labelMaxCarSlots
            // 
            this.labelMaxCarSlots.AutoSize = true;
            this.labelMaxCarSlots.Location = new System.Drawing.Point(43, 132);
            this.labelMaxCarSlots.Name = "labelMaxCarSlots";
            this.labelMaxCarSlots.Size = new System.Drawing.Size(105, 13);
            this.labelMaxCarSlots.TabIndex = 21;
            this.labelMaxCarSlots.Text = "Vacantes de coches";
            // 
            // controlServerName
            // 
            this.controlServerName.Location = new System.Drawing.Point(195, 36);
            this.controlServerName.Name = "controlServerName";
            this.controlServerName.Size = new System.Drawing.Size(202, 20);
            this.controlServerName.TabIndex = 14;
            // 
            // controlSpectatorPassword
            // 
            this.controlSpectatorPassword.Location = new System.Drawing.Point(195, 105);
            this.controlSpectatorPassword.Name = "controlSpectatorPassword";
            this.controlSpectatorPassword.Size = new System.Drawing.Size(202, 20);
            this.controlSpectatorPassword.TabIndex = 20;
            // 
            // labelServerName
            // 
            this.labelServerName.AutoSize = true;
            this.labelServerName.Location = new System.Drawing.Point(43, 39);
            this.labelServerName.Name = "labelServerName";
            this.labelServerName.Size = new System.Drawing.Size(44, 13);
            this.labelServerName.TabIndex = 13;
            this.labelServerName.Text = "Nombre";
            // 
            // controlPassword
            // 
            this.controlPassword.Location = new System.Drawing.Point(195, 82);
            this.controlPassword.Name = "controlPassword";
            this.controlPassword.Size = new System.Drawing.Size(202, 20);
            this.controlPassword.TabIndex = 18;
            // 
            // labelAdminPassword
            // 
            this.labelAdminPassword.AutoSize = true;
            this.labelAdminPassword.Location = new System.Drawing.Point(43, 62);
            this.labelAdminPassword.Name = "labelAdminPassword";
            this.labelAdminPassword.Size = new System.Drawing.Size(141, 13);
            this.labelAdminPassword.TabIndex = 15;
            this.labelAdminPassword.Text = "Contraseña de administrador";
            // 
            // controlAdminPassword
            // 
            this.controlAdminPassword.Location = new System.Drawing.Point(195, 59);
            this.controlAdminPassword.Name = "controlAdminPassword";
            this.controlAdminPassword.Size = new System.Drawing.Size(202, 20);
            this.controlAdminPassword.TabIndex = 16;
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(43, 85);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(104, 13);
            this.labelPassword.TabIndex = 17;
            this.labelPassword.Text = "Contraseña Corredor";
            // 
            // labelSpectatorPassword
            // 
            this.labelSpectatorPassword.AutoSize = true;
            this.labelSpectatorPassword.Location = new System.Drawing.Point(43, 108);
            this.labelSpectatorPassword.Name = "labelSpectatorPassword";
            this.labelSpectatorPassword.Size = new System.Drawing.Size(118, 13);
            this.labelSpectatorPassword.TabIndex = 19;
            this.labelSpectatorPassword.Text = "Contraseña Espectador";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.controlMaxConn);
            this.groupBox2.Controls.Add(this.controlLANDiscovery);
            this.groupBox2.Controls.Add(this.controlRegisterToLobby1);
            this.groupBox2.Controls.Add(this.controlRegisterToLobby0);
            this.groupBox2.Controls.Add(this.labelMaxConn);
            this.groupBox2.Location = new System.Drawing.Point(120, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(330, 100);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Server";
            // 
            // controlMaxConn
            // 
            this.controlMaxConn.Location = new System.Drawing.Point(37, 51);
            this.controlMaxConn.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.controlMaxConn.Name = "controlMaxConn";
            this.controlMaxConn.Size = new System.Drawing.Size(94, 20);
            this.controlMaxConn.TabIndex = 8;
            // 
            // controlLANDiscovery
            // 
            this.controlLANDiscovery.AutoSize = true;
            this.controlLANDiscovery.Location = new System.Drawing.Point(154, 35);
            this.controlLANDiscovery.Name = "controlLANDiscovery";
            this.controlLANDiscovery.Size = new System.Drawing.Size(123, 17);
            this.controlLANDiscovery.TabIndex = 9;
            this.controlLANDiscovery.Text = "Descubrimiento LAN";
            this.controlLANDiscovery.UseVisualStyleBackColor = true;
            // 
            // controlRegisterToLobby1
            // 
            this.controlRegisterToLobby1.AutoSize = true;
            this.controlRegisterToLobby1.Location = new System.Drawing.Point(221, 54);
            this.controlRegisterToLobby1.Name = "controlRegisterToLobby1";
            this.controlRegisterToLobby1.Size = new System.Drawing.Size(60, 17);
            this.controlRegisterToLobby1.TabIndex = 11;
            this.controlRegisterToLobby1.TabStop = true;
            this.controlRegisterToLobby1.Text = "Público";
            this.controlRegisterToLobby1.UseVisualStyleBackColor = true;
            // 
            // controlRegisterToLobby0
            // 
            this.controlRegisterToLobby0.AutoSize = true;
            this.controlRegisterToLobby0.Location = new System.Drawing.Point(154, 54);
            this.controlRegisterToLobby0.Name = "controlRegisterToLobby0";
            this.controlRegisterToLobby0.Size = new System.Drawing.Size(61, 17);
            this.controlRegisterToLobby0.TabIndex = 10;
            this.controlRegisterToLobby0.TabStop = true;
            this.controlRegisterToLobby0.Text = "Privado";
            this.controlRegisterToLobby0.UseVisualStyleBackColor = true;
            // 
            // labelMaxConn
            // 
            this.labelMaxConn.AutoSize = true;
            this.labelMaxConn.Location = new System.Drawing.Point(34, 35);
            this.labelMaxConn.Name = "labelMaxConn";
            this.labelMaxConn.Size = new System.Drawing.Size(97, 13);
            this.labelMaxConn.TabIndex = 7;
            this.labelMaxConn.Text = "Máximo de clientes";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.controlTCP);
            this.groupBox1.Controls.Add(this.controlUDP);
            this.groupBox1.Controls.Add(this.labelUDP);
            this.groupBox1.Controls.Add(this.labelTCP);
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(100, 100);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Puertos";
            // 
            // controlTCP
            // 
            this.controlTCP.Location = new System.Drawing.Point(48, 58);
            this.controlTCP.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.controlTCP.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.controlTCP.Name = "controlTCP";
            this.controlTCP.Size = new System.Drawing.Size(43, 20);
            this.controlTCP.TabIndex = 5;
            this.controlTCP.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // controlUDP
            // 
            this.controlUDP.Location = new System.Drawing.Point(47, 32);
            this.controlUDP.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.controlUDP.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.controlUDP.Name = "controlUDP";
            this.controlUDP.Size = new System.Drawing.Size(44, 20);
            this.controlUDP.TabIndex = 3;
            this.controlUDP.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // labelUDP
            // 
            this.labelUDP.AutoSize = true;
            this.labelUDP.Location = new System.Drawing.Point(11, 34);
            this.labelUDP.Name = "labelUDP";
            this.labelUDP.Size = new System.Drawing.Size(30, 13);
            this.labelUDP.TabIndex = 2;
            this.labelUDP.Text = "UDP";
            // 
            // labelTCP
            // 
            this.labelTCP.AutoSize = true;
            this.labelTCP.Location = new System.Drawing.Point(11, 61);
            this.labelTCP.Name = "labelTCP";
            this.labelTCP.Size = new System.Drawing.Size(28, 13);
            this.labelTCP.TabIndex = 5;
            this.labelTCP.Text = "TCP";
            // 
            // Configuracion
            // 
            this.Configuracion.Controls.Add(this.groupBox5);
            this.Configuracion.Controls.Add(this.groupBox4);
            this.Configuracion.Location = new System.Drawing.Point(4, 25);
            this.Configuracion.Name = "Configuracion";
            this.Configuracion.Padding = new System.Windows.Forms.Padding(3);
            this.Configuracion.Size = new System.Drawing.Size(450, 310);
            this.Configuracion.TabIndex = 1;
            this.Configuracion.Text = "Configuración";
            this.Configuracion.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.controlShortFormationLap);
            this.groupBox5.Controls.Add(this.controlAllowAutoDQ);
            this.groupBox5.Controls.Add(this.controlRadomizeTrackWhenEmpty);
            this.groupBox5.Controls.Add(this.controlIsRaceLocked);
            this.groupBox5.Controls.Add(this.controlDumpLeaderboard);
            this.groupBox5.Location = new System.Drawing.Point(10, 222);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(442, 88);
            this.groupBox5.TabIndex = 38;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Miscelaneos";
            // 
            // controlShortFormationLap
            // 
            this.controlShortFormationLap.AutoSize = true;
            this.controlShortFormationLap.Location = new System.Drawing.Point(220, 35);
            this.controlShortFormationLap.Name = "controlShortFormationLap";
            this.controlShortFormationLap.Size = new System.Drawing.Size(186, 17);
            this.controlShortFormationLap.TabIndex = 55;
            this.controlShortFormationLap.Text = "Vuelta de calentamiento completa";
            this.controlShortFormationLap.UseVisualStyleBackColor = true;
            // 
            // controlAllowAutoDQ
            // 
            this.controlAllowAutoDQ.AutoSize = true;
            this.controlAllowAutoDQ.Location = new System.Drawing.Point(220, 15);
            this.controlAllowAutoDQ.Name = "controlAllowAutoDQ";
            this.controlAllowAutoDQ.Size = new System.Drawing.Size(126, 17);
            this.controlAllowAutoDQ.TabIndex = 54;
            this.controlAllowAutoDQ.Text = "Auto desclasificación";
            this.controlAllowAutoDQ.UseVisualStyleBackColor = true;
            // 
            // controlRadomizeTrackWhenEmpty
            // 
            this.controlRadomizeTrackWhenEmpty.AutoSize = true;
            this.controlRadomizeTrackWhenEmpty.Location = new System.Drawing.Point(30, 55);
            this.controlRadomizeTrackWhenEmpty.Name = "controlRadomizeTrackWhenEmpty";
            this.controlRadomizeTrackWhenEmpty.Size = new System.Drawing.Size(135, 17);
            this.controlRadomizeTrackWhenEmpty.TabIndex = 53;
            this.controlRadomizeTrackWhenEmpty.Text = "Pista aleatoria al vaciar";
            this.controlRadomizeTrackWhenEmpty.UseVisualStyleBackColor = true;
            // 
            // controlIsRaceLocked
            // 
            this.controlIsRaceLocked.AutoSize = true;
            this.controlIsRaceLocked.Location = new System.Drawing.Point(30, 35);
            this.controlIsRaceLocked.Name = "controlIsRaceLocked";
            this.controlIsRaceLocked.Size = new System.Drawing.Size(159, 17);
            this.controlIsRaceLocked.TabIndex = 52;
            this.controlIsRaceLocked.Text = "Sin accesos durante carrera";
            this.controlIsRaceLocked.UseVisualStyleBackColor = true;
            // 
            // controlDumpLeaderboard
            // 
            this.controlDumpLeaderboard.AutoSize = true;
            this.controlDumpLeaderboard.Location = new System.Drawing.Point(30, 15);
            this.controlDumpLeaderboard.Name = "controlDumpLeaderboard";
            this.controlDumpLeaderboard.Size = new System.Drawing.Size(166, 17);
            this.controlDumpLeaderboard.TabIndex = 51;
            this.controlDumpLeaderboard.Text = "Guardar tabla de clasificación";
            this.controlDumpLeaderboard.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.valueMedals);
            this.groupBox4.Controls.Add(this.valueRacecraft);
            this.groupBox4.Controls.Add(this.valueSafety);
            this.groupBox4.Controls.Add(this.controlRacecraft);
            this.groupBox4.Controls.Add(this.controlSafety);
            this.groupBox4.Controls.Add(this.controlMedals);
            this.groupBox4.Controls.Add(this.labelRacecraft);
            this.groupBox4.Controls.Add(this.labelSafety);
            this.groupBox4.Controls.Add(this.labelMedals);
            this.groupBox4.Location = new System.Drawing.Point(10, 10);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(440, 210);
            this.groupBox4.TabIndex = 37;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Requisitos";
            // 
            // valueMedals
            // 
            this.valueMedals.AutoSize = true;
            this.valueMedals.Location = new System.Drawing.Point(380, 145);
            this.valueMedals.Name = "valueMedals";
            this.valueMedals.Size = new System.Drawing.Size(13, 13);
            this.valueMedals.TabIndex = 51;
            this.valueMedals.Text = "0";
            this.valueMedals.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valueRacecraft
            // 
            this.valueRacecraft.AutoSize = true;
            this.valueRacecraft.Location = new System.Drawing.Point(380, 80);
            this.valueRacecraft.Name = "valueRacecraft";
            this.valueRacecraft.Size = new System.Drawing.Size(16, 13);
            this.valueRacecraft.TabIndex = 50;
            this.valueRacecraft.Text = "-1";
            this.valueRacecraft.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valueSafety
            // 
            this.valueSafety.AutoSize = true;
            this.valueSafety.Location = new System.Drawing.Point(380, 16);
            this.valueSafety.Name = "valueSafety";
            this.valueSafety.Size = new System.Drawing.Size(16, 13);
            this.valueSafety.TabIndex = 49;
            this.valueSafety.Text = "-1";
            this.valueSafety.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // controlRacecraft
            // 
            this.controlRacecraft.Location = new System.Drawing.Point(28, 96);
            this.controlRacecraft.Maximum = 99;
            this.controlRacecraft.Minimum = -1;
            this.controlRacecraft.Name = "controlRacecraft";
            this.controlRacecraft.Size = new System.Drawing.Size(368, 45);
            this.controlRacecraft.TabIndex = 48;
            this.controlRacecraft.ValueChanged += new System.EventHandler(this.controlRacecraft_ValueChanged);
            // 
            // controlSafety
            // 
            this.controlSafety.Location = new System.Drawing.Point(28, 32);
            this.controlSafety.Maximum = 99;
            this.controlSafety.Minimum = -1;
            this.controlSafety.Name = "controlSafety";
            this.controlSafety.Size = new System.Drawing.Size(368, 45);
            this.controlSafety.TabIndex = 47;
            this.controlSafety.ValueChanged += new System.EventHandler(this.controlSafety_ValueChanged);
            // 
            // controlMedals
            // 
            this.controlMedals.LargeChange = 1;
            this.controlMedals.Location = new System.Drawing.Point(28, 161);
            this.controlMedals.Maximum = 3;
            this.controlMedals.Name = "controlMedals";
            this.controlMedals.Size = new System.Drawing.Size(368, 45);
            this.controlMedals.TabIndex = 46;
            this.controlMedals.ValueChanged += new System.EventHandler(this.controlMedals_ValueChanged);
            // 
            // labelRacecraft
            // 
            this.labelRacecraft.AutoSize = true;
            this.labelRacecraft.Location = new System.Drawing.Point(25, 80);
            this.labelRacecraft.Name = "labelRacecraft";
            this.labelRacecraft.Size = new System.Drawing.Size(87, 13);
            this.labelRacecraft.TabIndex = 36;
            this.labelRacecraft.Text = "Racecraft (-1,99)";
            // 
            // labelSafety
            // 
            this.labelSafety.AutoSize = true;
            this.labelSafety.Location = new System.Drawing.Point(25, 16);
            this.labelSafety.Name = "labelSafety";
            this.labelSafety.Size = new System.Drawing.Size(73, 13);
            this.labelSafety.TabIndex = 35;
            this.labelSafety.Text = "Safety (-1, 99)";
            // 
            // labelMedals
            // 
            this.labelMedals.AutoSize = true;
            this.labelMedals.Location = new System.Drawing.Point(25, 145);
            this.labelMedals.Name = "labelMedals";
            this.labelMedals.Size = new System.Drawing.Size(49, 13);
            this.labelMedals.TabIndex = 34;
            this.labelMedals.Text = "Medallas";
            // 
            // Evento
            // 
            this.Evento.Controls.Add(this.groupBox8);
            this.Evento.Controls.Add(this.groupBox7);
            this.Evento.Controls.Add(this.groupBox6);
            this.Evento.Location = new System.Drawing.Point(4, 25);
            this.Evento.Name = "Evento";
            this.Evento.Size = new System.Drawing.Size(450, 310);
            this.Evento.TabIndex = 2;
            this.Evento.Text = "Evento";
            this.Evento.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.controlTrack);
            this.groupBox8.Location = new System.Drawing.Point(10, 10);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(190, 55);
            this.groupBox8.TabIndex = 28;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Pista";
            // 
            // controlTrack
            // 
            this.controlTrack.FormattingEnabled = true;
            this.controlTrack.Location = new System.Drawing.Point(13, 19);
            this.controlTrack.Name = "controlTrack";
            this.controlTrack.Size = new System.Drawing.Size(167, 21);
            this.controlTrack.TabIndex = 30;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.controlPostRaceSeconds);
            this.groupBox7.Controls.Add(this.controlPostQualySeconds);
            this.groupBox7.Controls.Add(this.controlSessionOverTimeSeconds);
            this.groupBox7.Controls.Add(this.controlPreRaceWaitingSeconds);
            this.groupBox7.Controls.Add(this.labelPostRaceSeconds);
            this.groupBox7.Controls.Add(this.labelPostQualySeconds);
            this.groupBox7.Controls.Add(this.labelSessionOverTimeSeconds);
            this.groupBox7.Controls.Add(this.labelPreRaceWaitingSeconds);
            this.groupBox7.Location = new System.Drawing.Point(10, 63);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(190, 247);
            this.groupBox7.TabIndex = 27;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Entretiempos (segundos)";
            // 
            // controlPostRaceSeconds
            // 
            this.controlPostRaceSeconds.Location = new System.Drawing.Point(50, 183);
            this.controlPostRaceSeconds.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.controlPostRaceSeconds.Name = "controlPostRaceSeconds";
            this.controlPostRaceSeconds.Size = new System.Drawing.Size(94, 20);
            this.controlPostRaceSeconds.TabIndex = 28;
            // 
            // controlPostQualySeconds
            // 
            this.controlPostQualySeconds.Location = new System.Drawing.Point(50, 144);
            this.controlPostQualySeconds.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.controlPostQualySeconds.Name = "controlPostQualySeconds";
            this.controlPostQualySeconds.Size = new System.Drawing.Size(94, 20);
            this.controlPostQualySeconds.TabIndex = 27;
            // 
            // controlSessionOverTimeSeconds
            // 
            this.controlSessionOverTimeSeconds.Location = new System.Drawing.Point(50, 105);
            this.controlSessionOverTimeSeconds.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.controlSessionOverTimeSeconds.Name = "controlSessionOverTimeSeconds";
            this.controlSessionOverTimeSeconds.Size = new System.Drawing.Size(94, 20);
            this.controlSessionOverTimeSeconds.TabIndex = 26;
            // 
            // controlPreRaceWaitingSeconds
            // 
            this.controlPreRaceWaitingSeconds.Location = new System.Drawing.Point(50, 66);
            this.controlPreRaceWaitingSeconds.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.controlPreRaceWaitingSeconds.Name = "controlPreRaceWaitingSeconds";
            this.controlPreRaceWaitingSeconds.Size = new System.Drawing.Size(94, 20);
            this.controlPreRaceWaitingSeconds.TabIndex = 25;
            // 
            // labelPostRaceSeconds
            // 
            this.labelPostRaceSeconds.AutoSize = true;
            this.labelPostRaceSeconds.Location = new System.Drawing.Point(34, 167);
            this.labelPostRaceSeconds.Name = "labelPostRaceSeconds";
            this.labelPostRaceSeconds.Size = new System.Drawing.Size(102, 13);
            this.labelPostRaceSeconds.TabIndex = 24;
            this.labelPostRaceSeconds.Text = "Tiempo post Carrera";
            // 
            // labelPostQualySeconds
            // 
            this.labelPostQualySeconds.AutoSize = true;
            this.labelPostQualySeconds.Location = new System.Drawing.Point(34, 128);
            this.labelPostQualySeconds.Name = "labelPostQualySeconds";
            this.labelPostQualySeconds.Size = new System.Drawing.Size(127, 13);
            this.labelPostQualySeconds.TabIndex = 23;
            this.labelPostQualySeconds.Text = "Tiempo post Clasificación";
            // 
            // labelSessionOverTimeSeconds
            // 
            this.labelSessionOverTimeSeconds.AutoSize = true;
            this.labelSessionOverTimeSeconds.Location = new System.Drawing.Point(34, 89);
            this.labelSessionOverTimeSeconds.Name = "labelSessionOverTimeSeconds";
            this.labelSessionOverTimeSeconds.Size = new System.Drawing.Size(118, 13);
            this.labelSessionOverTimeSeconds.TabIndex = 22;
            this.labelSessionOverTimeSeconds.Text = "Tiempo de vuelta a box";
            // 
            // labelPreRaceWaitingSeconds
            // 
            this.labelPreRaceWaitingSeconds.AutoSize = true;
            this.labelPreRaceWaitingSeconds.Location = new System.Drawing.Point(34, 50);
            this.labelPreRaceWaitingSeconds.Name = "labelPreRaceWaitingSeconds";
            this.labelPreRaceWaitingSeconds.Size = new System.Drawing.Size(96, 13);
            this.labelPreRaceWaitingSeconds.TabIndex = 21;
            this.labelPreRaceWaitingSeconds.Text = "Tiempo pre carrera";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.valueWeatherRandomness);
            this.groupBox6.Controls.Add(this.valueRain);
            this.groupBox6.Controls.Add(this.valueCloudLevel);
            this.groupBox6.Controls.Add(this.valueAmbientTemp);
            this.groupBox6.Controls.Add(this.controlWeatherRandomness);
            this.groupBox6.Controls.Add(this.controlRain);
            this.groupBox6.Controls.Add(this.controlCloudLevel);
            this.groupBox6.Controls.Add(this.controlAmbientTemp);
            this.groupBox6.Controls.Add(this.labelAmbientTemp);
            this.groupBox6.Controls.Add(this.labelRain);
            this.groupBox6.Controls.Add(this.labelWeatherRandomness);
            this.groupBox6.Controls.Add(this.labelCloudLevel);
            this.groupBox6.Location = new System.Drawing.Point(206, 10);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(244, 300);
            this.groupBox6.TabIndex = 21;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Climatología";
            // 
            // valueWeatherRandomness
            // 
            this.valueWeatherRandomness.AutoSize = true;
            this.valueWeatherRandomness.Location = new System.Drawing.Point(204, 226);
            this.valueWeatherRandomness.Name = "valueWeatherRandomness";
            this.valueWeatherRandomness.Size = new System.Drawing.Size(16, 13);
            this.valueWeatherRandomness.TabIndex = 53;
            this.valueWeatherRandomness.Text = "-1";
            this.valueWeatherRandomness.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valueRain
            // 
            this.valueRain.AutoSize = true;
            this.valueRain.Location = new System.Drawing.Point(204, 165);
            this.valueRain.Name = "valueRain";
            this.valueRain.Size = new System.Drawing.Size(16, 13);
            this.valueRain.TabIndex = 52;
            this.valueRain.Text = "-1";
            this.valueRain.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valueCloudLevel
            // 
            this.valueCloudLevel.AutoSize = true;
            this.valueCloudLevel.Location = new System.Drawing.Point(204, 103);
            this.valueCloudLevel.Name = "valueCloudLevel";
            this.valueCloudLevel.Size = new System.Drawing.Size(16, 13);
            this.valueCloudLevel.TabIndex = 51;
            this.valueCloudLevel.Text = "-1";
            this.valueCloudLevel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valueAmbientTemp
            // 
            this.valueAmbientTemp.AutoSize = true;
            this.valueAmbientTemp.Location = new System.Drawing.Point(204, 34);
            this.valueAmbientTemp.Name = "valueAmbientTemp";
            this.valueAmbientTemp.Size = new System.Drawing.Size(16, 13);
            this.valueAmbientTemp.TabIndex = 50;
            this.valueAmbientTemp.Text = "-1";
            this.valueAmbientTemp.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // controlWeatherRandomness
            // 
            this.controlWeatherRandomness.LargeChange = 1;
            this.controlWeatherRandomness.Location = new System.Drawing.Point(33, 242);
            this.controlWeatherRandomness.Maximum = 7;
            this.controlWeatherRandomness.Name = "controlWeatherRandomness";
            this.controlWeatherRandomness.Size = new System.Drawing.Size(187, 45);
            this.controlWeatherRandomness.TabIndex = 28;
            this.controlWeatherRandomness.ValueChanged += new System.EventHandler(this.controlWeatherRandomness_ValueChanged);
            // 
            // controlRain
            // 
            this.controlRain.LargeChange = 1;
            this.controlRain.Location = new System.Drawing.Point(33, 178);
            this.controlRain.Name = "controlRain";
            this.controlRain.Size = new System.Drawing.Size(187, 45);
            this.controlRain.TabIndex = 27;
            this.controlRain.ValueChanged += new System.EventHandler(this.controlRain_ValueChanged);
            // 
            // controlCloudLevel
            // 
            this.controlCloudLevel.LargeChange = 1;
            this.controlCloudLevel.Location = new System.Drawing.Point(33, 114);
            this.controlCloudLevel.Name = "controlCloudLevel";
            this.controlCloudLevel.Size = new System.Drawing.Size(187, 45);
            this.controlCloudLevel.TabIndex = 26;
            this.controlCloudLevel.ValueChanged += new System.EventHandler(this.controlCloudLevel_ValueChanged);
            // 
            // controlAmbientTemp
            // 
            this.controlAmbientTemp.LargeChange = 1;
            this.controlAmbientTemp.Location = new System.Drawing.Point(33, 50);
            this.controlAmbientTemp.Maximum = 30;
            this.controlAmbientTemp.Minimum = 5;
            this.controlAmbientTemp.Name = "controlAmbientTemp";
            this.controlAmbientTemp.Size = new System.Drawing.Size(187, 45);
            this.controlAmbientTemp.TabIndex = 25;
            this.controlAmbientTemp.Value = 5;
            this.controlAmbientTemp.ValueChanged += new System.EventHandler(this.controlAmbientTemp_ValueChanged);
            // 
            // labelAmbientTemp
            // 
            this.labelAmbientTemp.AutoSize = true;
            this.labelAmbientTemp.Location = new System.Drawing.Point(30, 34);
            this.labelAmbientTemp.Name = "labelAmbientTemp";
            this.labelAmbientTemp.Size = new System.Drawing.Size(113, 13);
            this.labelAmbientTemp.TabIndex = 20;
            this.labelAmbientTemp.Text = "Temperatura ambiente";
            // 
            // labelRain
            // 
            this.labelRain.AutoSize = true;
            this.labelRain.Location = new System.Drawing.Point(30, 162);
            this.labelRain.Name = "labelRain";
            this.labelRain.Size = new System.Drawing.Size(33, 13);
            this.labelRain.TabIndex = 22;
            this.labelRain.Text = "Luvia";
            // 
            // labelWeatherRandomness
            // 
            this.labelWeatherRandomness.AutoSize = true;
            this.labelWeatherRandomness.Location = new System.Drawing.Point(30, 226);
            this.labelWeatherRandomness.Name = "labelWeatherRandomness";
            this.labelWeatherRandomness.Size = new System.Drawing.Size(66, 13);
            this.labelWeatherRandomness.TabIndex = 23;
            this.labelWeatherRandomness.Text = "Aleatoriedad";
            // 
            // labelCloudLevel
            // 
            this.labelCloudLevel.AutoSize = true;
            this.labelCloudLevel.Location = new System.Drawing.Point(30, 98);
            this.labelCloudLevel.Name = "labelCloudLevel";
            this.labelCloudLevel.Size = new System.Drawing.Size(78, 13);
            this.labelCloudLevel.TabIndex = 21;
            this.labelCloudLevel.Text = "Nivel de nubes";
            // 
            // FinDeSemana
            // 
            this.FinDeSemana.Controls.Add(this.controlSessionsTable);
            this.FinDeSemana.Controls.Add(this.groupBox9);
            this.FinDeSemana.Location = new System.Drawing.Point(4, 25);
            this.FinDeSemana.Name = "FinDeSemana";
            this.FinDeSemana.Size = new System.Drawing.Size(450, 310);
            this.FinDeSemana.TabIndex = 5;
            this.FinDeSemana.Text = "Fin de semana";
            this.FinDeSemana.UseVisualStyleBackColor = true;
            // 
            // controlSessionsTable
            // 
            this.controlSessionsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.controlSessionsTable.Location = new System.Drawing.Point(167, 16);
            this.controlSessionsTable.Name = "controlSessionsTable";
            this.controlSessionsTable.Size = new System.Drawing.Size(283, 294);
            this.controlSessionsTable.TabIndex = 29;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.controlDayOfWeekend);
            this.groupBox9.Controls.Add(this.controlEliminarSesion);
            this.groupBox9.Controls.Add(this.controlActualizarSesion);
            this.groupBox9.Controls.Add(this.controlAgregarSesion);
            this.groupBox9.Controls.Add(this.controlSessionDuration);
            this.groupBox9.Controls.Add(this.labelSessionDuration);
            this.groupBox9.Controls.Add(this.controlSessionType);
            this.groupBox9.Controls.Add(this.controlTimeMultiplier);
            this.groupBox9.Controls.Add(this.controlHourOfDay);
            this.groupBox9.Controls.Add(this.labelSessionType);
            this.groupBox9.Controls.Add(this.labelTimeMultiplier);
            this.groupBox9.Controls.Add(this.labelDayOfWeekend);
            this.groupBox9.Controls.Add(this.labelHourOfDay);
            this.groupBox9.Location = new System.Drawing.Point(10, 10);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(151, 300);
            this.groupBox9.TabIndex = 28;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Sesión";
            // 
            // controlDayOfWeekend
            // 
            this.controlDayOfWeekend.FormattingEnabled = true;
            this.controlDayOfWeekend.Location = new System.Drawing.Point(34, 77);
            this.controlDayOfWeekend.Name = "controlDayOfWeekend";
            this.controlDayOfWeekend.Size = new System.Drawing.Size(94, 21);
            this.controlDayOfWeekend.TabIndex = 36;
            // 
            // controlEliminarSesion
            // 
            this.controlEliminarSesion.Location = new System.Drawing.Point(49, 265);
            this.controlEliminarSesion.Name = "controlEliminarSesion";
            this.controlEliminarSesion.Size = new System.Drawing.Size(61, 23);
            this.controlEliminarSesion.TabIndex = 35;
            this.controlEliminarSesion.Text = "Eliminar";
            this.controlEliminarSesion.UseVisualStyleBackColor = true;
            // 
            // controlActualizarSesion
            // 
            this.controlActualizarSesion.Location = new System.Drawing.Point(79, 236);
            this.controlActualizarSesion.Name = "controlActualizarSesion";
            this.controlActualizarSesion.Size = new System.Drawing.Size(61, 23);
            this.controlActualizarSesion.TabIndex = 34;
            this.controlActualizarSesion.Text = "Actualizar";
            this.controlActualizarSesion.UseVisualStyleBackColor = true;
            // 
            // controlAgregarSesion
            // 
            this.controlAgregarSesion.Location = new System.Drawing.Point(15, 236);
            this.controlAgregarSesion.Name = "controlAgregarSesion";
            this.controlAgregarSesion.Size = new System.Drawing.Size(61, 23);
            this.controlAgregarSesion.TabIndex = 4;
            this.controlAgregarSesion.Text = "Agregar";
            this.controlAgregarSesion.UseVisualStyleBackColor = true;
            this.controlAgregarSesion.Click += new System.EventHandler(this.controlAgregarSesion_Click);
            // 
            // controlSessionDuration
            // 
            this.controlSessionDuration.Location = new System.Drawing.Point(34, 195);
            this.controlSessionDuration.Maximum = new decimal(new int[] {
            1440,
            0,
            0,
            0});
            this.controlSessionDuration.Name = "controlSessionDuration";
            this.controlSessionDuration.Size = new System.Drawing.Size(94, 20);
            this.controlSessionDuration.TabIndex = 33;
            // 
            // labelSessionDuration
            // 
            this.labelSessionDuration.AutoSize = true;
            this.labelSessionDuration.Location = new System.Drawing.Point(18, 179);
            this.labelSessionDuration.Name = "labelSessionDuration";
            this.labelSessionDuration.Size = new System.Drawing.Size(98, 13);
            this.labelSessionDuration.TabIndex = 32;
            this.labelSessionDuration.Text = "Duración de sesión";
            // 
            // controlSessionType
            // 
            this.controlSessionType.FormattingEnabled = true;
            this.controlSessionType.Location = new System.Drawing.Point(34, 155);
            this.controlSessionType.Name = "controlSessionType";
            this.controlSessionType.Size = new System.Drawing.Size(94, 21);
            this.controlSessionType.TabIndex = 31;
            // 
            // controlTimeMultiplier
            // 
            this.controlTimeMultiplier.Location = new System.Drawing.Point(34, 117);
            this.controlTimeMultiplier.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.controlTimeMultiplier.Name = "controlTimeMultiplier";
            this.controlTimeMultiplier.Size = new System.Drawing.Size(94, 20);
            this.controlTimeMultiplier.TabIndex = 27;
            // 
            // controlHourOfDay
            // 
            this.controlHourOfDay.Location = new System.Drawing.Point(34, 39);
            this.controlHourOfDay.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.controlHourOfDay.Name = "controlHourOfDay";
            this.controlHourOfDay.Size = new System.Drawing.Size(94, 20);
            this.controlHourOfDay.TabIndex = 25;
            // 
            // labelSessionType
            // 
            this.labelSessionType.AutoSize = true;
            this.labelSessionType.Location = new System.Drawing.Point(18, 140);
            this.labelSessionType.Name = "labelSessionType";
            this.labelSessionType.Size = new System.Drawing.Size(76, 13);
            this.labelSessionType.TabIndex = 24;
            this.labelSessionType.Text = "Tipo de sesión";
            // 
            // labelTimeMultiplier
            // 
            this.labelTimeMultiplier.AutoSize = true;
            this.labelTimeMultiplier.Location = new System.Drawing.Point(18, 101);
            this.labelTimeMultiplier.Name = "labelTimeMultiplier";
            this.labelTimeMultiplier.Size = new System.Drawing.Size(115, 13);
            this.labelTimeMultiplier.TabIndex = 23;
            this.labelTimeMultiplier.Text = "Multiplicador de tiempo";
            // 
            // labelDayOfWeekend
            // 
            this.labelDayOfWeekend.AutoSize = true;
            this.labelDayOfWeekend.Location = new System.Drawing.Point(18, 62);
            this.labelDayOfWeekend.Name = "labelDayOfWeekend";
            this.labelDayOfWeekend.Size = new System.Drawing.Size(23, 13);
            this.labelDayOfWeekend.TabIndex = 22;
            this.labelDayOfWeekend.Text = "Dia";
            // 
            // labelHourOfDay
            // 
            this.labelHourOfDay.AutoSize = true;
            this.labelHourOfDay.Location = new System.Drawing.Point(18, 23);
            this.labelHourOfDay.Name = "labelHourOfDay";
            this.labelHourOfDay.Size = new System.Drawing.Size(66, 13);
            this.labelHourOfDay.TabIndex = 21;
            this.labelHourOfDay.Text = "Hora del día";
            // 
            // Reglas
            // 
            this.Reglas.Controls.Add(this.label12);
            this.Reglas.Controls.Add(this.tyreSetCount);
            this.Reglas.Controls.Add(this.label13);
            this.Reglas.Controls.Add(this.isMandatoryPitstopSwapDriverRequired);
            this.Reglas.Controls.Add(this.label11);
            this.Reglas.Controls.Add(this.isMandatoryPitstopTyreChangeRequired);
            this.Reglas.Controls.Add(this.label10);
            this.Reglas.Controls.Add(this.isMandatoryPitstopRefuellingRequired);
            this.Reglas.Controls.Add(this.label9);
            this.Reglas.Controls.Add(this.maxDriversCount);
            this.Reglas.Controls.Add(this.label8);
            this.Reglas.Controls.Add(this.maxTotalDrivingTime);
            this.Reglas.Controls.Add(this.label3);
            this.Reglas.Controls.Add(this.mandatoryPitstopCount);
            this.Reglas.Controls.Add(this.label2);
            this.Reglas.Controls.Add(this.driverStintTimeSec);
            this.Reglas.Controls.Add(this.pitWindowLengthSec);
            this.Reglas.Controls.Add(this.isRefuellingTimeFixed);
            this.Reglas.Controls.Add(this.label7);
            this.Reglas.Controls.Add(this.isRefuellingAllowedInRace);
            this.Reglas.Controls.Add(this.label6);
            this.Reglas.Controls.Add(this.label5);
            this.Reglas.Controls.Add(this.label4);
            this.Reglas.Controls.Add(this.qualifyStandingType);
            this.Reglas.Controls.Add(this.label1);
            this.Reglas.Location = new System.Drawing.Point(4, 25);
            this.Reglas.Name = "Reglas";
            this.Reglas.Size = new System.Drawing.Size(450, 310);
            this.Reglas.TabIndex = 4;
            this.Reglas.Text = "Reglas";
            this.Reglas.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(404, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 13);
            this.label12.TabIndex = 77;
            this.label12.Text = "-1";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tyreSetCount
            // 
            this.tyreSetCount.LargeChange = 1;
            this.tyreSetCount.Location = new System.Drawing.Point(288, 28);
            this.tyreSetCount.Maximum = 50;
            this.tyreSetCount.Minimum = 1;
            this.tyreSetCount.Name = "tyreSetCount";
            this.tyreSetCount.Size = new System.Drawing.Size(125, 45);
            this.tyreSetCount.TabIndex = 76;
            this.tyreSetCount.Value = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(285, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 75;
            this.label13.Text = "Sests de llantas";
            // 
            // isMandatoryPitstopSwapDriverRequired
            // 
            this.isMandatoryPitstopSwapDriverRequired.FormattingEnabled = true;
            this.isMandatoryPitstopSwapDriverRequired.Items.AddRange(new object[] {
            "true",
            "false"});
            this.isMandatoryPitstopSwapDriverRequired.Location = new System.Drawing.Point(212, 275);
            this.isMandatoryPitstopSwapDriverRequired.Name = "isMandatoryPitstopSwapDriverRequired";
            this.isMandatoryPitstopSwapDriverRequired.Size = new System.Drawing.Size(45, 21);
            this.isMandatoryPitstopSwapDriverRequired.TabIndex = 74;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 278);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(190, 13);
            this.label11.TabIndex = 73;
            this.label11.Text = "Cambio de piloto obligatorio por parada";
            // 
            // isMandatoryPitstopTyreChangeRequired
            // 
            this.isMandatoryPitstopTyreChangeRequired.FormattingEnabled = true;
            this.isMandatoryPitstopTyreChangeRequired.Items.AddRange(new object[] {
            "true",
            "false"});
            this.isMandatoryPitstopTyreChangeRequired.Location = new System.Drawing.Point(211, 248);
            this.isMandatoryPitstopTyreChangeRequired.Name = "isMandatoryPitstopTyreChangeRequired";
            this.isMandatoryPitstopTyreChangeRequired.Size = new System.Drawing.Size(45, 21);
            this.isMandatoryPitstopTyreChangeRequired.TabIndex = 72;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 251);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(195, 13);
            this.label10.TabIndex = 71;
            this.label10.Text = "Cambio de llantas obligatorio por parada";
            // 
            // isMandatoryPitstopRefuellingRequired
            // 
            this.isMandatoryPitstopRefuellingRequired.FormattingEnabled = true;
            this.isMandatoryPitstopRefuellingRequired.Items.AddRange(new object[] {
            "true",
            "false"});
            this.isMandatoryPitstopRefuellingRequired.Location = new System.Drawing.Point(212, 222);
            this.isMandatoryPitstopRefuellingRequired.Name = "isMandatoryPitstopRefuellingRequired";
            this.isMandatoryPitstopRefuellingRequired.Size = new System.Drawing.Size(45, 21);
            this.isMandatoryPitstopRefuellingRequired.TabIndex = 70;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 225);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(160, 13);
            this.label9.TabIndex = 69;
            this.label9.Text = "Repostaje obligatorio por parada";
            // 
            // maxDriversCount
            // 
            this.maxDriversCount.Location = new System.Drawing.Point(192, 193);
            this.maxDriversCount.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.maxDriversCount.Name = "maxDriversCount";
            this.maxDriversCount.Size = new System.Drawing.Size(64, 20);
            this.maxDriversCount.TabIndex = 68;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 193);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(142, 13);
            this.label8.TabIndex = 67;
            this.label8.Text = "Máximo de pilotos por coche";
            // 
            // maxTotalDrivingTime
            // 
            this.maxTotalDrivingTime.Location = new System.Drawing.Point(192, 167);
            this.maxTotalDrivingTime.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.maxTotalDrivingTime.Name = "maxTotalDrivingTime";
            this.maxTotalDrivingTime.Size = new System.Drawing.Size(64, 20);
            this.maxTotalDrivingTime.TabIndex = 66;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 65;
            this.label3.Text = "Tiempo máximo por piloto";
            // 
            // mandatoryPitstopCount
            // 
            this.mandatoryPitstopCount.Location = new System.Drawing.Point(192, 140);
            this.mandatoryPitstopCount.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.mandatoryPitstopCount.Name = "mandatoryPitstopCount";
            this.mandatoryPitstopCount.Size = new System.Drawing.Size(64, 20);
            this.mandatoryPitstopCount.TabIndex = 64;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 13);
            this.label2.TabIndex = 63;
            this.label2.Text = "El tiempo de repostaje es fijo";
            // 
            // driverStintTimeSec
            // 
            this.driverStintTimeSec.Location = new System.Drawing.Point(163, 58);
            this.driverStintTimeSec.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.driverStintTimeSec.Name = "driverStintTimeSec";
            this.driverStintTimeSec.Size = new System.Drawing.Size(94, 20);
            this.driverStintTimeSec.TabIndex = 62;
            // 
            // pitWindowLengthSec
            // 
            this.pitWindowLengthSec.Location = new System.Drawing.Point(163, 33);
            this.pitWindowLengthSec.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.pitWindowLengthSec.Name = "pitWindowLengthSec";
            this.pitWindowLengthSec.Size = new System.Drawing.Size(94, 20);
            this.pitWindowLengthSec.TabIndex = 61;
            // 
            // isRefuellingTimeFixed
            // 
            this.isRefuellingTimeFixed.FormattingEnabled = true;
            this.isRefuellingTimeFixed.Items.AddRange(new object[] {
            "true",
            "false"});
            this.isRefuellingTimeFixed.Location = new System.Drawing.Point(212, 113);
            this.isRefuellingTimeFixed.Name = "isRefuellingTimeFixed";
            this.isRefuellingTimeFixed.Size = new System.Drawing.Size(45, 21);
            this.isRefuellingTimeFixed.TabIndex = 60;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 116);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(141, 13);
            this.label7.TabIndex = 59;
            this.label7.Text = "El tiempo de repostaje es fijo";
            // 
            // isRefuellingAllowedInRace
            // 
            this.isRefuellingAllowedInRace.FormattingEnabled = true;
            this.isRefuellingAllowedInRace.Items.AddRange(new object[] {
            "true",
            "false"});
            this.isRefuellingAllowedInRace.Location = new System.Drawing.Point(212, 87);
            this.isRefuellingAllowedInRace.Name = "isRefuellingAllowedInRace";
            this.isRefuellingAllowedInRace.Size = new System.Drawing.Size(45, 21);
            this.isRefuellingAllowedInRace.TabIndex = 58;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(160, 13);
            this.label6.TabIndex = 57;
            this.label6.Text = "Se permite repostar en la carrera";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 13);
            this.label5.TabIndex = 54;
            this.label5.Text = "Stint máximo por piloto";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 51;
            this.label4.Text = "Ventana de parada";
            // 
            // qualifyStandingType
            // 
            this.qualifyStandingType.FormattingEnabled = true;
            this.qualifyStandingType.Items.AddRange(new object[] {
            "Vuleta rápida",
            "Promedio"});
            this.qualifyStandingType.Location = new System.Drawing.Point(211, 6);
            this.qualifyStandingType.Name = "qualifyStandingType";
            this.qualifyStandingType.Size = new System.Drawing.Size(45, 21);
            this.qualifyStandingType.TabIndex = 31;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tipo de calificación";
            // 
            // Ayudas
            // 
            this.Ayudas.Controls.Add(this.disableAutoLights);
            this.Ayudas.Controls.Add(this.label23);
            this.Ayudas.Controls.Add(this.disableAutoWiper);
            this.Ayudas.Controls.Add(this.label22);
            this.Ayudas.Controls.Add(this.disableAutoEngineStart);
            this.Ayudas.Controls.Add(this.label21);
            this.Ayudas.Controls.Add(this.disableAutoClutch);
            this.Ayudas.Controls.Add(this.label20);
            this.Ayudas.Controls.Add(this.disableAutoGear);
            this.Ayudas.Controls.Add(this.label19);
            this.Ayudas.Controls.Add(this.disableAutoPitLimiter);
            this.Ayudas.Controls.Add(this.label18);
            this.Ayudas.Controls.Add(this.disableIdealLine);
            this.Ayudas.Controls.Add(this.label17);
            this.Ayudas.Controls.Add(this.disableAutosteer);
            this.Ayudas.Controls.Add(this.label16);
            this.Ayudas.Controls.Add(this.label14);
            this.Ayudas.Controls.Add(this.stabilityControlLevelMax);
            this.Ayudas.Controls.Add(this.label15);
            this.Ayudas.Location = new System.Drawing.Point(4, 25);
            this.Ayudas.Name = "Ayudas";
            this.Ayudas.Size = new System.Drawing.Size(450, 310);
            this.Ayudas.TabIndex = 6;
            this.Ayudas.Text = "Ayudas";
            this.Ayudas.UseVisualStyleBackColor = true;
            // 
            // disableAutoLights
            // 
            this.disableAutoLights.FormattingEnabled = true;
            this.disableAutoLights.Items.AddRange(new object[] {
            "true",
            "false"});
            this.disableAutoLights.Location = new System.Drawing.Point(216, 261);
            this.disableAutoLights.Name = "disableAutoLights";
            this.disableAutoLights.Size = new System.Drawing.Size(45, 21);
            this.disableAutoLights.TabIndex = 96;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(15, 264);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(150, 13);
            this.label23.TabIndex = 95;
            this.label23.Text = "Deshabilitar luces automáticas";
            // 
            // disableAutoWiper
            // 
            this.disableAutoWiper.FormattingEnabled = true;
            this.disableAutoWiper.Items.AddRange(new object[] {
            "true",
            "false"});
            this.disableAutoWiper.Location = new System.Drawing.Point(216, 234);
            this.disableAutoWiper.Name = "disableAutoWiper";
            this.disableAutoWiper.Size = new System.Drawing.Size(45, 21);
            this.disableAutoWiper.TabIndex = 94;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(15, 237);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(199, 13);
            this.label22.TabIndex = 93;
            this.label22.Text = "Deshabilitar limpiaparabrisas automáticos";
            // 
            // disableAutoEngineStart
            // 
            this.disableAutoEngineStart.FormattingEnabled = true;
            this.disableAutoEngineStart.Items.AddRange(new object[] {
            "true",
            "false"});
            this.disableAutoEngineStart.Location = new System.Drawing.Point(216, 208);
            this.disableAutoEngineStart.Name = "disableAutoEngineStart";
            this.disableAutoEngineStart.Size = new System.Drawing.Size(45, 21);
            this.disableAutoEngineStart.TabIndex = 92;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 211);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(170, 13);
            this.label21.TabIndex = 91;
            this.label21.Text = "Deshabilitar encendido automático";
            // 
            // disableAutoClutch
            // 
            this.disableAutoClutch.FormattingEnabled = true;
            this.disableAutoClutch.Items.AddRange(new object[] {
            "true",
            "false"});
            this.disableAutoClutch.Location = new System.Drawing.Point(216, 182);
            this.disableAutoClutch.Name = "disableAutoClutch";
            this.disableAutoClutch.Size = new System.Drawing.Size(45, 21);
            this.disableAutoClutch.TabIndex = 90;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(15, 185);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(149, 13);
            this.label20.TabIndex = 89;
            this.label20.Text = "Deshabilitar clutch automático";
            // 
            // disableAutoGear
            // 
            this.disableAutoGear.FormattingEnabled = true;
            this.disableAutoGear.Items.AddRange(new object[] {
            "true",
            "false"});
            this.disableAutoGear.Location = new System.Drawing.Point(216, 156);
            this.disableAutoGear.Name = "disableAutoGear";
            this.disableAutoGear.Size = new System.Drawing.Size(45, 21);
            this.disableAutoGear.TabIndex = 88;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 159);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(164, 13);
            this.label19.TabIndex = 87;
            this.label19.Text = "Deshabilitar cambios automáticos";
            // 
            // disableAutoPitLimiter
            // 
            this.disableAutoPitLimiter.FormattingEnabled = true;
            this.disableAutoPitLimiter.Items.AddRange(new object[] {
            "true",
            "false"});
            this.disableAutoPitLimiter.Location = new System.Drawing.Point(216, 130);
            this.disableAutoPitLimiter.Name = "disableAutoPitLimiter";
            this.disableAutoPitLimiter.Size = new System.Drawing.Size(45, 21);
            this.disableAutoPitLimiter.TabIndex = 86;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(15, 133);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(187, 13);
            this.label18.TabIndex = 85;
            this.label18.Text = "Deshabilitar limitador de pit automático";
            // 
            // disableIdealLine
            // 
            this.disableIdealLine.FormattingEnabled = true;
            this.disableIdealLine.Items.AddRange(new object[] {
            "true",
            "false"});
            this.disableIdealLine.Location = new System.Drawing.Point(216, 104);
            this.disableIdealLine.Name = "disableIdealLine";
            this.disableIdealLine.Size = new System.Drawing.Size(45, 21);
            this.disableIdealLine.TabIndex = 84;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 107);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(134, 13);
            this.label17.TabIndex = 83;
            this.label17.Text = "Deshabilitar trazada optima";
            // 
            // disableAutosteer
            // 
            this.disableAutosteer.FormattingEnabled = true;
            this.disableAutosteer.Items.AddRange(new object[] {
            "true",
            "false"});
            this.disableAutosteer.Location = new System.Drawing.Point(216, 77);
            this.disableAutosteer.Name = "disableAutosteer";
            this.disableAutosteer.Size = new System.Drawing.Size(45, 21);
            this.disableAutosteer.TabIndex = 82;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 80);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(163, 13);
            this.label16.TabIndex = 81;
            this.label16.Text = "Deshabilitar dirección automática";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(134, 13);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(16, 13);
            this.label14.TabIndex = 80;
            this.label14.Text = "-1";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // stabilityControlLevelMax
            // 
            this.stabilityControlLevelMax.LargeChange = 1;
            this.stabilityControlLevelMax.Location = new System.Drawing.Point(18, 32);
            this.stabilityControlLevelMax.Maximum = 20;
            this.stabilityControlLevelMax.Name = "stabilityControlLevelMax";
            this.stabilityControlLevelMax.Size = new System.Drawing.Size(125, 45);
            this.stabilityControlLevelMax.TabIndex = 79;
            this.stabilityControlLevelMax.Value = 5;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(108, 13);
            this.label15.TabIndex = 78;
            this.label15.Text = "Control de estabilidad";
            // 
            // process1
            // 
            this.process1.StartInfo.Domain = "";
            this.process1.StartInfo.LoadUserProfile = false;
            this.process1.StartInfo.Password = null;
            this.process1.StartInfo.StandardErrorEncoding = null;
            this.process1.StartInfo.StandardOutputEncoding = null;
            this.process1.StartInfo.UserName = "";
            this.process1.SynchronizingObject = this;
            // 
            // controlIniciar
            // 
            this.controlIniciar.Location = new System.Drawing.Point(131, 350);
            this.controlIniciar.Name = "controlIniciar";
            this.controlIniciar.Size = new System.Drawing.Size(218, 23);
            this.controlIniciar.TabIndex = 1;
            this.controlIniciar.Text = "Iniciar";
            this.controlIniciar.UseVisualStyleBackColor = true;
            this.controlIniciar.Click += new System.EventHandler(this.controlIniciar_Click);
            // 
            // controlDetener
            // 
            this.controlDetener.Location = new System.Drawing.Point(355, 350);
            this.controlDetener.Name = "controlDetener";
            this.controlDetener.Size = new System.Drawing.Size(109, 23);
            this.controlDetener.TabIndex = 2;
            this.controlDetener.Text = "Detener";
            this.controlDetener.UseVisualStyleBackColor = true;
            this.controlDetener.Click += new System.EventHandler(this.controlDetener_Click);
            // 
            // controlActualizar
            // 
            this.controlActualizar.Location = new System.Drawing.Point(14, 350);
            this.controlActualizar.Name = "controlActualizar";
            this.controlActualizar.Size = new System.Drawing.Size(109, 23);
            this.controlActualizar.TabIndex = 3;
            this.controlActualizar.Text = "Actualizar";
            this.controlActualizar.UseVisualStyleBackColor = true;
            this.controlActualizar.Click += new System.EventHandler(this.controlActualizar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 381);
            this.Controls.Add(this.controlActualizar);
            this.Controls.Add(this.controlDetener);
            this.Controls.Add(this.controlIniciar);
            this.Controls.Add(this.tabs);
            this.Name = "Form1";
            this.Text = "ACCServerGUI";
            this.tabs.ResumeLayout(false);
            this.Conexiones.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlMaxCarSlots)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlMaxConn)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlTCP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlUDP)).EndInit();
            this.Configuracion.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlRacecraft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlSafety)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlMedals)).EndInit();
            this.Evento.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlPostRaceSeconds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlPostQualySeconds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlSessionOverTimeSeconds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlPreRaceWaitingSeconds)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlWeatherRandomness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlRain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlCloudLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlAmbientTemp)).EndInit();
            this.FinDeSemana.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.controlSessionsTable)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlSessionDuration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlTimeMultiplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlHourOfDay)).EndInit();
            this.Reglas.ResumeLayout(false);
            this.Reglas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tyreSetCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxDriversCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxTotalDrivingTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mandatoryPitstopCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.driverStintTimeSec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pitWindowLengthSec)).EndInit();
            this.Ayudas.ResumeLayout(false);
            this.Ayudas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stabilityControlLevelMax)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage Conexiones;
        private System.Windows.Forms.TabPage Configuracion;
        private System.Windows.Forms.TabPage Evento;
        private System.Windows.Forms.TabPage Reglas;
        private System.Windows.Forms.Label labelUDP;
        private System.Windows.Forms.RadioButton controlRegisterToLobby0;
        private System.Windows.Forms.RadioButton controlRegisterToLobby1;
        private System.Windows.Forms.Label labelTCP;
        private System.Windows.Forms.Label labelMaxConn;
        private System.Windows.Forms.CheckBox controlLANDiscovery;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label labelRacecraft;
        private System.Windows.Forms.Label labelSafety;
        private System.Windows.Forms.Label labelMedals;
        private System.Windows.Forms.CheckBox controlShortFormationLap;
        private System.Windows.Forms.CheckBox controlAllowAutoDQ;
        private System.Windows.Forms.CheckBox controlRadomizeTrackWhenEmpty;
        private System.Windows.Forms.CheckBox controlIsRaceLocked;
        private System.Windows.Forms.CheckBox controlDumpLeaderboard;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label labelRain;
        private System.Windows.Forms.Label labelCloudLevel;
        private System.Windows.Forms.Label labelAmbientTemp;
        private System.Windows.Forms.TabPage FinDeSemana;
        private System.Windows.Forms.TrackBar controlAmbientTemp;
        private System.Windows.Forms.TrackBar controlCloudLevel;
        private System.Windows.Forms.NumericUpDown controlUDP;
        private System.Windows.Forms.NumericUpDown controlMaxConn;
        private System.Windows.Forms.NumericUpDown controlTCP;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label labelMaxCarSlots;
        private System.Windows.Forms.TextBox controlServerName;
        private System.Windows.Forms.TextBox controlSpectatorPassword;
        private System.Windows.Forms.Label labelServerName;
        private System.Windows.Forms.TextBox controlPassword;
        private System.Windows.Forms.Label labelAdminPassword;
        private System.Windows.Forms.TextBox controlAdminPassword;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Label labelSpectatorPassword;
        private System.Windows.Forms.TrackBar controlRacecraft;
        private System.Windows.Forms.TrackBar controlSafety;
        private System.Windows.Forms.TrackBar controlMedals;
        private System.Windows.Forms.TrackBar controlRain;
        private System.Windows.Forms.Label valueRacecraft;
        private System.Windows.Forms.Label valueSafety;
        private System.Windows.Forms.Label valueMedals;
        private System.Diagnostics.Process process1;
        private System.Windows.Forms.Button controlDetener;
        private System.Windows.Forms.Button controlIniciar;
        private System.Windows.Forms.Button controlActualizar;
        private System.Windows.Forms.NumericUpDown controlMaxCarSlots;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox controlTrack;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.NumericUpDown controlPostRaceSeconds;
        private System.Windows.Forms.NumericUpDown controlPostQualySeconds;
        private System.Windows.Forms.NumericUpDown controlSessionOverTimeSeconds;
        private System.Windows.Forms.NumericUpDown controlPreRaceWaitingSeconds;
        private System.Windows.Forms.Label labelPostRaceSeconds;
        private System.Windows.Forms.Label labelPostQualySeconds;
        private System.Windows.Forms.Label labelSessionOverTimeSeconds;
        private System.Windows.Forms.Label labelPreRaceWaitingSeconds;
        private System.Windows.Forms.TrackBar controlWeatherRandomness;
        private System.Windows.Forms.Label labelWeatherRandomness;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.NumericUpDown controlTimeMultiplier;
        private System.Windows.Forms.NumericUpDown controlHourOfDay;
        private System.Windows.Forms.Label labelSessionType;
        private System.Windows.Forms.Label labelTimeMultiplier;
        private System.Windows.Forms.Label labelDayOfWeekend;
        private System.Windows.Forms.Label labelHourOfDay;
        private System.Windows.Forms.ComboBox controlSessionType;
        private System.Windows.Forms.Button controlEliminarSesion;
        private System.Windows.Forms.Button controlActualizarSesion;
        private System.Windows.Forms.Button controlAgregarSesion;
        private System.Windows.Forms.NumericUpDown controlSessionDuration;
        private System.Windows.Forms.Label labelSessionDuration;
        private System.Windows.Forms.ComboBox controlDayOfWeekend;
        private System.Windows.Forms.DataGridView controlSessionsTable;
        private System.Windows.Forms.Label valueWeatherRandomness;
        private System.Windows.Forms.Label valueRain;
        private System.Windows.Forms.Label valueCloudLevel;
        private System.Windows.Forms.Label valueAmbientTemp;
        private System.Windows.Forms.TabPage Ayudas;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TrackBar tyreSetCount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox isMandatoryPitstopSwapDriverRequired;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox isMandatoryPitstopTyreChangeRequired;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox isMandatoryPitstopRefuellingRequired;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown maxDriversCount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown maxTotalDrivingTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown mandatoryPitstopCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown driverStintTimeSec;
        private System.Windows.Forms.NumericUpDown pitWindowLengthSec;
        private System.Windows.Forms.ComboBox isRefuellingTimeFixed;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox isRefuellingAllowedInRace;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox qualifyStandingType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox disableAutoLights;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox disableAutoWiper;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox disableAutoEngineStart;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox disableAutoClutch;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox disableAutoGear;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox disableAutoPitLimiter;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox disableIdealLine;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox disableAutosteer;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TrackBar stabilityControlLevelMax;
        private System.Windows.Forms.Label label15;
    }
}

