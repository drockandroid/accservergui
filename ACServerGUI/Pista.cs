﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACCServerGUI
{
    //Propiedades de pista
    class Pista
    {
        private String _nombre;
        private String _value;
        public Pista() { }
        public Pista(String nombre,
                    String value) {
            Nombre = nombre;
            Value = value;
        }
        /// <summary>
        /// Nombre completo de la pista
        /// </summary>
        public String Nombre { get => _nombre; set => _nombre = value; }
        /// <summary>
        /// Nombre clave del dato
        /// </summary>
        public String Value { get => _value; set => _value = value; }
    }
}
