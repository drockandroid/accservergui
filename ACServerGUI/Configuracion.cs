﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACCServerGUI
{
    public class Configuracion
    {
        // Propiedades de settings.json
        private String _serverName;
        private String _adminPassword;
        private int _trackMedalsRequirement;
        private int _safetyRatingRequirement;
        private int _racecraftRatingRequirement;
        private String _password;
        private String _spectatorPassword;
        private int _maxCarSlots;
        private int _dumpLeaderboards;
        private int _isRaceLocked;
        private int _randomizeTrackWhenEmpty;
        private String _centralEntryListPath;
        private int _allowAutoDQ;
        private int _shortFormationLap;
        private int _dumpEntryList;
        public Configuracion() { }
        public Configuracion(String serverName,
                            String adminPassword,
                            int trackMedalsRequirement,
                            int safetyRatingRequirement,
                            int racecraftRatingRequirement,
                            String password,
                            String spectatorPassword,
                            int maxCarSlots,
                            int dumpLeaderboards,
                            int isRaceLocked,
                            int randomizeTrackWhenEmpty,
                            String centralEntryListPath,
                            int allowAutoDQ,
                            int shortFormationLap,
                            int dumpEntryList)
        {
            this.serverName = serverName;
            this.adminPassword = adminPassword;
            this.trackMedalsRequirement = trackMedalsRequirement;
            this.safetyRatingRequirement = safetyRatingRequirement;
            this.racecraftRatingRequirement = racecraftRatingRequirement;
            this.password = password;
            this.spectatorPassword = spectatorPassword;
            this.maxCarSlots = maxCarSlots;
            this.dumpLeaderboards = dumpLeaderboards;
            this.isRaceLocked = isRaceLocked;
            this.randomizeTrackWhenEmpty = randomizeTrackWhenEmpty;
            this.centralEntryListPath = centralEntryListPath;
            this.allowAutoDQ = allowAutoDQ;
            this.shortFormationLap = shortFormationLap;
            this.dumpEntryList = dumpEntryList;
        }
        // Propiedades de settings.json
        /// <summary>
        /// Nombre del servidor
        /// </summary>
        public String serverName { get => _serverName; set => _serverName = value; }
        /// <summary>
        /// Contraseña para ingresar al servidor con rol de administrador
        /// </summary>
        public String adminPassword { get => _adminPassword; set => _adminPassword = value; }
        /// <summary>
        /// Medallas requeridas:
        /// 0, 1, 2, 3.
        /// </summary>
        public int trackMedalsRequirement { get => _trackMedalsRequirement; set => _trackMedalsRequirement = value; }
        /// <summary>
        /// Nivel de SA requerido:
        /// -1, 0, ... , 99.
        /// </summary>
        public int safetyRatingRequirement { get => _safetyRatingRequirement; set => _safetyRatingRequirement = value; }
        /// <summary>
        /// Nivel de RC requerido:
        /// -1, 0, ... , 99.
        /// </summary>
        public int racecraftRatingRequirement { get => _racecraftRatingRequirement; set => _racecraftRatingRequirement = value; }
        /// <summary>
        /// Contraseña para entrar al servidor
        /// </summary>
        public String password { get => _password; set => _password = value; }
        /// <summary>
        /// Contraseña para entrar al servodor como espectador
        /// </summary>
        public String spectatorPassword { get => _spectatorPassword; set => _spectatorPassword = value; }
        /// <summary>
        /// Máximo de aparcamientos a utilizar, si se sobrepasa la capacidad máxima que posee el circuito, este valor se sobreescribira con esa capacidad
        /// </summary>
        public int maxCarSlots { get => _maxCarSlots; set => _maxCarSlots = value; }
        /// <summary>
        /// Guardar la tabla de clasificación.
        /// Cuando el valor es 1, todas tablas de clasificación para cada sesión serán almacenadas en la capeta de "results"
        /// </summary>
        public int dumpLeaderboards { get => _dumpLeaderboards; set => _dumpLeaderboards = value; }
        /// <summary>
        /// Permitir entrar en servidor en sesión de carrera.
        /// Cuando el valor es 0, el servidor permitirá el ingreso
        /// </summary>
        public int isRaceLocked { get => _isRaceLocked; set => _isRaceLocked = value; }
        /// <summary>
        /// Pista aleatoria al vaciarse el servidor.
        /// Cuando el valor es 1, se cambiará la pista.
        /// </summary>
        public int randomizeTrackWhenEmpty { get => _randomizeTrackWhenEmpty; set => _randomizeTrackWhenEmpty = value; }
        /// <summary>
        /// Sobreescribe la ruta por defecto "cfg/entrylist.json", de esta forma multiples servidores pueden usar el mismo archivo de entrylist.json.
        /// Ejemplo con carpeta que contiene a entrylist.json: "C:/entradaPersonalizada/"
        /// </summary>
        public String centralEntryListPath { get => _centralEntryListPath; set => _centralEntryListPath = value; }
        /// <summary>
        /// Auto desclasificación de la sesión.
        /// Cuando el valor es 0, el servidor no descalificará sin embargo asignará un Stop&Go, así el Director de Carrera puede ver la acción y usar un /dq o /clear
        /// </summary>
        public int allowAutoDQ { get => _allowAutoDQ; set => _allowAutoDQ = value; }
        /// <summary>
        /// Modo de formación para carrera:
        /// Cuando el valor es 0, la formación será en la última curva.
        /// Cuando el valor es 1, la formación será toda una vuelta.
        /// </summary>
        public int shortFormationLap { get => _shortFormationLap; set => _shortFormationLap = value; }
        /// <summary>
        /// Guarda una entrylist al final de la sesión de clasificación.
        /// Cuando el valor es 0, no se almacenará la lista.
        /// </summary>
        public int dumpEntryList { get => _dumpEntryList; set => _dumpEntryList = value; }
    }
}