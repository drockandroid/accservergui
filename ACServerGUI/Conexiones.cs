﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACCServerGUI
{
    public class Conexiones
    {
        // Propiedades de configuration.json
        private int _udpPort;
        private int _tcpPort;
        private int _maxConnections;
        private int _lanDiscovery;
        private int _registerToLobby;
        private int _configVersion;
        public Conexiones() { }
        public Conexiones(int UDPPort,
                          int TCPPort,
                          int MaxConnections,
                          int LanDiscovery,
                          int RegisterToLobby,
                          int ConfigVersion)
        {
            this.udpPort = UDPPort;
            this.tcpPort = TCPPort;
            this.maxConnections = MaxConnections;
            this.lanDiscovery = LanDiscovery;
            this.registerToLobby = RegisterToLobby;
            this.configVersion = ConfigVersion;
        }
        /// <summary>
        /// </summary>
        /// <value>
        /// </value>
        public int udpPort { get => _udpPort; set => _udpPort = value; }
        public int tcpPort { get => _tcpPort; set => _tcpPort = value; }
        public int maxConnections { get => _maxConnections; set => _maxConnections = value; }
        public int lanDiscovery { get => _lanDiscovery; set => _lanDiscovery = value; }
        public int registerToLobby { get => _registerToLobby; set => _registerToLobby = value; }
        public int configVersion { get => _configVersion; set => _configVersion = value; }
    }
}