﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACCServerGUI
{
    class Session : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private int _hourOfDay;
        private int _dayOfWeekend;
        private double _timeMultiplier;
        private String _sessionType;
        private int _sessionDurationMinutes;
        public Session() { }
        public Session(int hourOfDay,
                        int dayOfWeekend,
                        double timeMultiplier,
                        String sessionType,
                        int sessionDurationMinutes)
        {
            HourOfDay = hourOfDay;
            DayOfWeekend = dayOfWeekend;
            TimeMultiplier = timeMultiplier;
            SessionType = sessionType;
            SessionDurationMinutes = sessionDurationMinutes;
        }
        public int HourOfDay {
            get => _hourOfDay;
            set
            {
                _hourOfDay = value;
                OnPropertyChanged("SessionType");
            }
        }
        public int DayOfWeekend {
            get => _dayOfWeekend;
            set
            {
                _dayOfWeekend = value;
                OnPropertyChanged("SessionType");
            }
        }
        public double TimeMultiplier {
            get => _timeMultiplier;
            set
            {
                _timeMultiplier = value;
                OnPropertyChanged("SessionType");
            }
        }
        public String SessionType {
            get => _sessionType;
            set
            {
                _sessionType = value;
                OnPropertyChanged("SessionType");
            }
        }
        public int SessionDurationMinutes {
            get => _sessionDurationMinutes;
            set
            {
                _sessionDurationMinutes = value;
                OnPropertyChanged("SessionType");
            }
        }
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
